<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('dashboard/','DashboardController@index')->name('dashboard.index');
Route::get('auth/login','CustomAuth\AuthController@index')->name('auth.login');
Route::get('auth/register','CustomAuth\AuthController@register')->name('auth.register');
Route::post('auth','CustomAuth\AuthController@auth')->name('auth.auth');
Route::resource('dashboard/petugas', 'Master\PetugasController');
Route::resource('dashboard/pelanggan', 'Master\PelangganController');
