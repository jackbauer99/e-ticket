<?php

Route::prefix('auth')
	->name('auth.')
	->group(function() {

	Route::get('logout', 'CustomAuth\AuthController@logout')->name('logout');
	
	Route::middleware(['not_login'])->group(function() {
		//Route::get('index', 'AuthController@index')->name('index');
		Route::get('login', 'CustomAuth\AuthController@index')->name('login');
		Route::post('login', 'CustomAuth\AuthController@auth')->name('auth');
		Route::get('register', 'CustomAuth\AuthController@register')->name('register');
		Route::post('register', 'CustomAuth\AuthController@store')->name('store');
	});
});

