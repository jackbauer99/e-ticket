<?php


Route::namespace('API')
	->name('API.')
	->group(function() {

		Route::resource('ticket', 'TicketControllerAPI');
		//Route::resource('ticketact','PengambilanTicketControllerAPI');
		Route::get('ticketact','PengambilanTicketControllerAPI@index')->name('ticketact.index');
		Route::get('ticketact/{ticketact}/{token}/create','PengambilanTicketControllerAPI@create')->name('ticketact.create');

});
