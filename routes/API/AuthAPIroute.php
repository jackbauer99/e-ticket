<?php

Route::prefix('auth')
    ->name('auth.API.')
    ->namespace('API')
	->group(function() {

	Route::get('logout', 'AuthControllerAPI@logout')->name('logout');
	
	Route::middleware(['not_login'])->group(function() {
		//Route::get('index', 'AuthController@index')->name('index');
		Route::get('login', 'AuthControllerAPI@index')->name('login');
		Route::post('login', 'AuthControllerAPI@auth')->name('auth');
		Route::get('register', 'AuthControllerAPI@register')->name('register');
		Route::post('register', 'AuthControllerAPI@store')->name('store');
	});
});

