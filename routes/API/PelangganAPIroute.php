<?php

Route::namespace('API')
    ->name('API.')
    ->group(function(){
        Route::resource('pelanggan', 'PelangganControllerAPI');
        Route::post('pelanggan/store','PelangganControllerAPI@store');
    })

?>