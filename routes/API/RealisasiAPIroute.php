<?php


Route::namespace('API')
	->name('API.')
	->group(function() {

		Route::get('realisasi/{realisasi}/{token}', 'RealisasiControllerAPI@index')->name('index');
		Route::post('realisasi/update/{realisasi}/{token}','RealisasiControllerAPI@update')->name('update');

});
