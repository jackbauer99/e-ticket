<?php

Route::prefix('master')
	->name('realisasi.')
	->group(function() {

	Route::middleware(['login'])->group(function() {
		//Route::get('index', 'AuthController@index')->name('index');
		Route::get('realisasi', 'Master\RealisasiController@index')->name('index');
		Route::get('realisasi/{realisasi}/edit', 'Master\RealisasiController@edit')->name('edit');
		Route::delete('realisasi/{realisasi}', 'Master\RealisasiController@destroy')->name('destroy');
		Route::put('realisasi/editAct/{realisasi}', 'Master\RealisasiController@editAct')->name('update');
		Route::get('realisasi/verification/{realisasi}','Master\RealisasiController@upVerification')->name('ver');
		Route::get('realisasi/downverification/{realisasi}','Master\RealisasiController@downVerification')->name('downver');
	});
});
