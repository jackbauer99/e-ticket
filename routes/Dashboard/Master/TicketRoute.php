<?php

Route::prefix('master')
	->group(function() {
	
	Route::middleware(['login'])->group(function() {
		//Route::get('index', 'AuthController@index')->name('index');
		Route::resource('ticket', 'Master\TicketController');
		Route::middleware(['ticketpermission'])->group(function(){
			Route::resource('ticketact','Master\PengambilanTicketController');
		});
	});
});

