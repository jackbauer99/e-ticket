<?php

Route::prefix('master')
	->group(function() {
	
	Route::middleware(['login'])->group(function() {
		Route::middleware(['dashboardpermission'])->group(function(){
			Route::resource('pelanggan', 'Master\PelangganController');
		});	
		//Route::get('index', 'AuthController@index')->name('index');
	});
});

