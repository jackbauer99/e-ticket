<?php

Route::prefix('master')
	->group(function() {
	
	Route::middleware(['login'])->group(function() {
		//Route::get('index', 'AuthController@index')->name('index');
		Route::middleware(['dashboardpermission'])->group(function(){
			Route::middleware(['petugaspermission'])->group(function(){
				Route::resource('petugas', 'Master\PetugasController');
			});

			Route::get('petugas/{petugas}/changePass', 'Master\PetugasController@changePass')->name('petugas.changePass');
			Route::put('petugas/password/{petugas}', 'Master\PetugasController@password')->name('petugas.password');
		});	
	});
});

