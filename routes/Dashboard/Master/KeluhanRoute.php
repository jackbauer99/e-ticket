<?php

Route::prefix('master')
	->group(function() {
	
	Route::middleware(['login'])->group(function() {
		//Route::get('index', 'AuthController@index')->name('index');
		Route::resource('keluhan', 'Master\KeluhanController');
		Route::get('keluhan/suggest','Master\PelangganController@autosuggest');
	});
});

