<?php

Route::get('/', function() {
	return redirect()->route('auth.login');
});

Route::middleware(['login'])
	->prefix('dashboard')
	->name('dashboard.')
	->group(function() {

	Route::get('/', 'DashboardController@index')->name('index');
	Route::middleware(['settingspermission'])->group(function(){
		Route::get('settings','DashboardController@getSettingsLayout')->name('settings');
		Route::put('update/{dashboard}','DashboardController@setSettings')->name('update');
		Route::get('loginactivity','DashboardController@setLoginActivity')->name('loginactivity');	
	});
});
