<?php

use Illuminate\Database\Seeder;
use App\Models\Petugas;
use Illuminate\Support\Facades\Hash;

use Faker\Factory as Faker;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /*Petugas::create([
            'role_id' => 2,
            'petugas_nip' => '099',
            'petugas_nama' => 'Adi',
            'petugas_jabatan' => 'HRD',
            'petugas_alamat' => 'Jagiran',
            'petugas_username' => '099',
            'petugas_password' => Hash::make('3467'),
        ]);*/
        
        /*$faker = Faker::create('id_ID');

        $jabatan = array();
        $jabatan = array(
            1 => 'Petugas Lapangan',
            2 => 'Petugas Kantor'
        );

        for($i = 1; $i <= 10; $i++) {
            $nip = date('Ymd').'/'.rand(90,250);
            $tanggal_lahir = rand(1950,2000).'-'.rand(1,12).'-'.rand(1,31);
            Petugas::create([
                'role_id' => 2,
                'petugas_nip' => $nip,
                'petugas_nama' => $faker->name,
                'petugas_jabatan' => $jabatan[rand(1,2)],
                'petugas_alamat' => $faker->address,
                'petugas_tanggal_lahir' => $tanggal_lahir,
                'petugas_username' => $nip,
                'petugas_password' => Hash::make($tanggal_lahir),
            ]);
        }*/
        Petugas::create([
            'role_id' => 1,
            'petugas_nip' => 'admin',
            'petugas_nama' => 'Nuggetz',
            'petugas_jabatan' => 'Admin',
            'petugas_alamat' => 'Jagiran',
            'petugas_username' => 'admin',
            'petugas_password' => Hash::make('supri'),
            'is_diganti' => 1,
        ]);
    }
}
