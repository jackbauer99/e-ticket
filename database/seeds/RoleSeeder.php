<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Role::create([
			'role_name' => 'Admin'
		]);

		Role::create([
			'role_name' => 'Petugas Kantor'
		]);

		Role::create([
			'role_name' => 'Petugas Lapangan'
        ]);
        
        Role::create([
            'role_name' => 'Pelanggan'
        ]);
    }
}
