<?php

use Illuminate\Database\Seeder;
use App\Models\Pelanggan;
use Illuminate\Support\Facades\Hash;

use Faker\Factory as Faker;

class PelangganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = Faker::create('id_ID');


        for($i = 1; $i <= 10; $i++) {
            $id = date('Ymd').rand(90,250);
            $tanggal_lahir = rand(1950,2000).'-'.rand(1,12).'-'.rand(1,31);
            Pelanggan::create([
                'role_id' => 3,
                'pelanggan_id' => $id,
                'pelanggan_nama' => $faker->name,
                'pelanggan_alamat' => $faker->address,
                'pelanggan_tanggal_lahir' => $tanggal_lahir,
                //'pelanggan_jk' => $faker->gender,
                'pelanggan_username' => $id,
                'pelanggan_password' => Hash::make($tanggal_lahir),
            ]);
        }
    }
}
