<?php

use Illuminate\Database\Seeder;
use App\Models\Settings;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Settings::create([
            'dashboard_name' => 'Sistem E-Ticket',
            //'login_image' => '/assets/settings/login.jpeg',
            //'register_image' => '/assets/settings/register.jpeg',
        ]);
    }
}
