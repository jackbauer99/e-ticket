<?php

use Illuminate\Database\Seeder;
use App\Models\Keluhan;

class KeluhanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Keluhan::create([
            'pelanggan_id' => 5,
            'keluhan_nama' => 'Bocor',
            'keluhan_tanggal' => rand(1950,2000).'-'.rand(1,12).'-'.rand(1,31),
        ]);
    }
}
