<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePetugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petugas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('role_id')->unsigned();
            $table->string('petugas_nip');
            $table->string('petugas_nama');
            $table->string('petugas_jabatan');
            $table->string('petugas_alamat')->nullable();
            $table->string('petugas_tanggal_lahir')->nullable();
            $table->string('petugas_jk')->nullable();
            $table->string('petugas_foto')->nullable();
            $table->string('petugas_username');
            $table->string('petugas_password');
            $table->integer('is_diganti');

            $table->foreign('role_id')->references('id')->on('role');

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petugas');
    }
}
