<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoginAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('login_agents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('petugas_id')->unsigned();
            $table->string('ipaddress');
            $table->string('operating_system');
            $table->string('webbrowser');
            $table->string('devices');
            $table->string('activity');
            $table->timestamp('last_login_date');
            $table->timestamps();

            $table->foreign('petugas_id')
            ->references('id')
            ->on('petugas')
            ->onDelete('restrict')
            ->onUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('login_agents');
    }
}
