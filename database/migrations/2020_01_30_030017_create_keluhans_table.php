<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeluhansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keluhan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pelanggan_id')->unsigned();
            $table->bigInteger('petugas_id')->unsigned();
            $table->bigInteger('ticket_id')->unsigned();
            $table->text('keluhan_nama');
            $table->date('keluhan_tanggal')->nullable();

            $table->foreign('pelanggan_id')
            ->references('id')
            ->on('pelanggan')
            ->onDelete('restrict')
            ->onUpdate('restrict');

            $table->foreign('ticket_id')
            ->references('id')
            ->on('tiket')
            ->onDelete('restrict')
            ->onUpdate('restrict');

            $table->foreign('petugas_id')
            ->references('id')
            ->on('petugas')
            ->onDelete('restrict')
            ->onUpdate('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keluhan');
    }
}
