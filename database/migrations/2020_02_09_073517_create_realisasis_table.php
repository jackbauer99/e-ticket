<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealisasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realisasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ticket_id')->unsigned();
            $table->bigInteger('petugas_id')->unsigned();
            $table->text('realisasi_nama')->nullable();
            $table->string('foto_realisasi_1')->nullable();
            $table->string('foto_realisasi_2')->nullable();
            $table->string('foto_realisasi_3')->nullable();
            $table->string('foto_realisasi_4')->nullable();
            $table->date('realisasi_tanggal')->nullable();
            $table->integer('is_realisasi');

            $table->foreign('petugas_id')
            ->references('id')
            ->on('petugas')
            ->onDelete('restrict')
            ->onUpdate('restrict');

            $table->foreign('ticket_id')
            ->references('id')
            ->on('tiket')
            ->onDelete('restrict')
            ->onUpdate('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realisasi');
    }
}
