<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelanggansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelanggan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('role_id')->unsigned();
            $table->string('pelanggan_id');
            $table->string('pelanggan_nama');
            $table->string('pelanggan_alamat')->nullable();
            $table->string('pelanggan_telepon')->nullable();
            $table->string('pelanggan_tanggal_lahir')->nullable();
            $table->string('pelanggan_jk')->nullable();
            $table->string('pelanggan_foto')->nullable();
            $table->string('pelanggan_username');
            $table->string('pelanggan_password');

            $table->foreign('role_id')->references('id')->on('role')->onDelete('cascade');

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            //$table->collation = 'utf8_unicode_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelanggan');
    }
}
