-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: eticket
-- ------------------------------------------------------
-- Server version	10.1.44-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `keluhan`
--

DROP TABLE IF EXISTS `keluhan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keluhan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pelanggan_id` bigint(20) unsigned NOT NULL,
  `petugas_id` bigint(20) unsigned NOT NULL,
  `ticket_id` bigint(20) unsigned NOT NULL,
  `keluhan_nama` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keluhan_tanggal` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `keluhan_pelanggan_id_foreign` (`pelanggan_id`),
  KEY `keluhan_ticket_id_foreign` (`ticket_id`),
  KEY `keluhan_petugas_id_foreign` (`petugas_id`),
  CONSTRAINT `keluhan_pelanggan_id_foreign` FOREIGN KEY (`pelanggan_id`) REFERENCES `pelanggan` (`id`),
  CONSTRAINT `keluhan_petugas_id_foreign` FOREIGN KEY (`petugas_id`) REFERENCES `petugas` (`id`),
  CONSTRAINT `keluhan_ticket_id_foreign` FOREIGN KEY (`ticket_id`) REFERENCES `tiket` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keluhan`
--

LOCK TABLES `keluhan` WRITE;
/*!40000 ALTER TABLE `keluhan` DISABLE KEYS */;
/*!40000 ALTER TABLE `keluhan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_agents`
--

DROP TABLE IF EXISTS `login_agents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_agents` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `petugas_id` bigint(20) unsigned NOT NULL,
  `ipaddress` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `operating_system` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `webbrowser` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `devices` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `login_agents_petugas_id_foreign` (`petugas_id`),
  CONSTRAINT `login_agents_petugas_id_foreign` FOREIGN KEY (`petugas_id`) REFERENCES `petugas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_agents`
--

LOCK TABLES `login_agents` WRITE;
/*!40000 ALTER TABLE `login_agents` DISABLE KEYS */;
INSERT INTO `login_agents` VALUES (1,1,'127.0.0.1','Linux','Chrome','WebKit','2020-05-18 02:06:37','2020-05-18 02:06:37','2020-05-18 02:06:37'),(2,2,'127.0.0.1','Linux','Chrome','WebKit','2020-05-18 02:08:19','2020-05-18 02:08:19','2020-05-18 02:08:19'),(3,1,'127.0.0.1','0','0','0','2020-05-18 02:11:29','2020-05-18 02:11:29','2020-05-18 02:11:29'),(4,1,'127.0.0.1','Linux','Chrome','WebKit','2020-05-18 02:30:17','2020-05-18 02:30:17','2020-05-18 02:30:17');
/*!40000 ALTER TABLE `login_agents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (115,'2014_10_12_000000_create_users_table',1),(116,'2014_10_12_100000_create_password_resets_table',1),(117,'2020_01_30_014704_create_roles_table',1),(118,'2020_01_30_020111_create_pelanggans_table',1),(119,'2020_01_30_024622_create_petugas_table',1),(120,'2020_01_30_030016_create_tickets_table',1),(121,'2020_01_30_030017_create_keluhans_table',1),(122,'2020_02_09_073517_create_realisasis_table',1),(123,'2020_02_13_125351_create_settings_table',1),(124,'2020_04_20_141211_create_token_logins_table',1),(125,'2020_05_18_002014_create_login_agents_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pelanggan`
--

DROP TABLE IF EXISTS `pelanggan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pelanggan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `pelanggan_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pelanggan_nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pelanggan_alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pelanggan_telepon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pelanggan_tanggal_lahir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pelanggan_jk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pelanggan_foto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pelanggan_username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pelanggan_password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pelanggan_role_id_foreign` (`role_id`),
  CONSTRAINT `pelanggan_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelanggan`
--

LOCK TABLES `pelanggan` WRITE;
/*!40000 ALTER TABLE `pelanggan` DISABLE KEYS */;
INSERT INTO `pelanggan` VALUES (1,3,'20200518123','Legawa Imam Putra','Ds. Orang No. 729, Lhokseumawe 62745, DIY',NULL,'1978-1-31',NULL,NULL,'20200518123','$2y$10$v/2Ou8VjRcRUF1ltoOG0LeZa7wNxteYmHJYO2S9JBoyZl2eIdpPVq',NULL,'2020-05-17 19:06:24','2020-05-17 19:06:24',NULL),(2,3,'20200518244','Ami Salimah Rahimah','Kpg. Pasteur No. 429, Administrasi Jakarta Utara 56982, KalBar',NULL,'1961-5-30',NULL,NULL,'20200518244','$2y$10$xqEbeEb.nLb4HiMppIawaOkEJJytcmhbq.nI1ZbZjJnC9m22H4PY6',NULL,'2020-05-17 19:06:24','2020-05-17 19:06:24',NULL),(3,3,'20200518111','Winda Syahrini Halimah S.IP','Jln. Rajawali No. 751, Sabang 64666, SulTeng',NULL,'1969-9-26',NULL,NULL,'20200518111','$2y$10$voUfvklBQLDI2myqYYIKQOLThFfFdlfc1.l2KZfhXA/BB6jhO1BFC',NULL,'2020-05-17 19:06:24','2020-05-17 19:06:24',NULL),(4,3,'20200518235','Winda Kuswandari S.Gz','Jr. Dipenogoro No. 240, Jambi 75646, DKI',NULL,'1996-2-7',NULL,NULL,'20200518235','$2y$10$Mn6kc9EY8O89dqE7.6sc1u93lVKaS/o1js6deKfbPbxSeOuZ/I1hW',NULL,'2020-05-17 19:06:24','2020-05-17 19:06:24',NULL),(5,3,'20200518245','Gawati Ira Nuraini','Jln. Juanda No. 722, Serang 93597, Gorontalo',NULL,'1996-4-19',NULL,NULL,'20200518245','$2y$10$zzI80yTNVc/HqaWmKlbWOukucUoMch/bmcoFvlcqZVGTJLYmmLuwu',NULL,'2020-05-17 19:06:24','2020-05-17 19:06:24',NULL),(6,3,'20200518247','Hilda Maimunah Andriani','Dk. Basoka Raya No. 257, Batu 27831, KalTim',NULL,'1996-9-29',NULL,NULL,'20200518247','$2y$10$3kk.Q649LhYDZgDn3oidCusFqEsrKvd/MY4jCVGU8mMxunDGdgRCa',NULL,'2020-05-17 19:06:24','2020-05-17 19:06:24',NULL),(7,3,'20200518216','Kania Winarsih M.M.','Jr. Cut Nyak Dien No. 850, Batam 23270, Aceh',NULL,'1992-8-14',NULL,NULL,'20200518216','$2y$10$kv9xbKtvvMCxRzcWLVC74upo2sOdgCRkEJjzYr5c4k30OIrTyjvES',NULL,'2020-05-17 19:06:25','2020-05-17 19:06:25',NULL),(8,3,'2020051894','Among Firgantoro','Jr. Suprapto No. 928, Pontianak 56128, Gorontalo',NULL,'1971-4-29',NULL,NULL,'2020051894','$2y$10$btdEsyYlcCSwDRhLYIh7d.7U/SMSQ5cj2gFZ6k9ymZ6SFVrucqlcu',NULL,'2020-05-17 19:06:25','2020-05-17 19:06:25',NULL),(9,3,'20200518152','Gilda Utami S.I.Kom','Ki. Samanhudi No. 98, Administrasi Jakarta Utara 10553, DIY',NULL,'1999-2-17',NULL,NULL,'20200518152','$2y$10$PFMsQDZrByKOofC0LOvOHufxCkMAhHfyEIn8E24aQXunJmJI2XeHK',NULL,'2020-05-17 19:06:25','2020-05-17 19:06:25',NULL),(10,3,'20200518155','Ami Usada','Ds. Gajah Mada No. 480, Pagar Alam 58740, SumBar',NULL,'1995-2-7',NULL,NULL,'20200518155','$2y$10$Fhdd5Ihl0f3j4NGo089G0e3IngoE.QxcwtfIn48CP.2nU4qnTXir6',NULL,'2020-05-17 19:06:25','2020-05-17 19:06:25',NULL);
/*!40000 ALTER TABLE `pelanggan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `petugas`
--

DROP TABLE IF EXISTS `petugas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `petugas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `petugas_nip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `petugas_nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `petugas_jabatan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `petugas_alamat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `petugas_tanggal_lahir` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `petugas_jk` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `petugas_foto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `petugas_username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `petugas_password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_diganti` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `petugas_role_id_foreign` (`role_id`),
  CONSTRAINT `petugas_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `petugas`
--

LOCK TABLES `petugas` WRITE;
/*!40000 ALTER TABLE `petugas` DISABLE KEYS */;
INSERT INTO `petugas` VALUES (1,1,'admin','Nuggetz','Admin','Jagiran',NULL,NULL,NULL,'admin','$2y$10$X.AQCppDMoim9q.5HUUbVedx9fun.iF5TurlUGxUW1HL1utoVmQm6',1,'Uue7NI69QAdL6JFMCzI6gXZTX4mP0x3Z3C14c9r3zvewxCXhsm8aY05HPt5P','2020-05-17 19:06:24','2020-05-17 19:06:24',NULL),(2,2,'PK2020051857','Paijo','Petugas Kantor','Jagiran Lor Wetan Kulon Progo','1991-09-09','L','public/assets/petugas/PK2020051857.jpg','PK2020051857','$2y$10$GvhliLWT94LBX0oq3crywu6jFLsWP6N3IpJizR932nZhEw0UOWm6K',0,'Jj6lJ8GRVi4ndpD6dZykf7zwMm8fH7qkCyYYyT9ftyamLgggzw6gvEtSbJGL','2020-05-17 19:08:02','2020-05-17 19:08:02',NULL);
/*!40000 ALTER TABLE `petugas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `realisasi`
--

DROP TABLE IF EXISTS `realisasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `realisasi` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_id` bigint(20) unsigned NOT NULL,
  `petugas_id` bigint(20) unsigned NOT NULL,
  `realisasi_nama` text COLLATE utf8mb4_unicode_ci,
  `foto_realisasi_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_realisasi_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_realisasi_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_realisasi_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `realisasi_tanggal` date DEFAULT NULL,
  `is_realisasi` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `realisasi_petugas_id_foreign` (`petugas_id`),
  KEY `realisasi_ticket_id_foreign` (`ticket_id`),
  CONSTRAINT `realisasi_petugas_id_foreign` FOREIGN KEY (`petugas_id`) REFERENCES `petugas` (`id`),
  CONSTRAINT `realisasi_ticket_id_foreign` FOREIGN KEY (`ticket_id`) REFERENCES `tiket` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `realisasi`
--

LOCK TABLES `realisasi` WRITE;
/*!40000 ALTER TABLE `realisasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `realisasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin','2020-05-17 19:06:24','2020-05-17 19:06:24'),(2,'Petugas Kantor','2020-05-17 19:06:24','2020-05-17 19:06:24'),(3,'Petugas Lapangan','2020-05-17 19:06:24','2020-05-17 19:06:24'),(4,'Pelanggan','2020-05-17 19:06:24','2020-05-17 19:06:24');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dashboard_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `register_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'Sistem E-Ticket','public/assets/settings/login.jpg',NULL,'2020-05-17 19:06:25','2020-05-17 19:42:36');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiket`
--

DROP TABLE IF EXISTS `tiket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiket` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no_ticket` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tiket_tanggal` date NOT NULL,
  `tiket_tanggal_realisasi` date DEFAULT NULL,
  `tiket_diambil` date DEFAULT NULL,
  `is_diambil` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiket`
--

LOCK TABLES `tiket` WRITE;
/*!40000 ALTER TABLE `tiket` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token_logins`
--

DROP TABLE IF EXISTS `token_logins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token_logins` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expired_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token_logins`
--

LOCK TABLES `token_logins` WRITE;
/*!40000 ALTER TABLE `token_logins` DISABLE KEYS */;
INSERT INTO `token_logins` VALUES (1,'635e2db0cf0e8bae8d6cc4715691c018','2020-05-18 02:11:29','2020-05-18 02:11:29','2020-05-19');
/*!40000 ALTER TABLE `token_logins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-18  9:44:36
