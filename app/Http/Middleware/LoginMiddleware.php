<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(@Auth::guard('pelanggan')->user()->role_id != 4
         && @Auth::guard('petugas')->user()->role_id != 1 
         && @Auth::guard('petugas')->user()->role_id != 2
         && @Auth::guard('petugas')->user()->role_id != 3)
		{
			return redirect()->route('auth.login');	
			//return abort(404);		
		}
        return $next($request);
    }
}
