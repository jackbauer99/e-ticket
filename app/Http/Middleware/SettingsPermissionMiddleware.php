<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class SettingsPermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(@Auth::guard('petugas')->check()) {

            if(@Auth::guard('petugas')->user()->petugas_username != "admin") {
                return redirect()->route('dashboard.index');
            }            
        }
        return $next($request);
    }
}
