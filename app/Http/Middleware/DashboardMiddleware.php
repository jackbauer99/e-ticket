<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class DashboardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(@Auth::guard('pelanggan')->check()) {

            return redirect()->route('dashboard.index');
            
        }
        
        return $next($request);
    }
}
