<?php

namespace App\Http\Controllers\CustomAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Jenssegers\Agent\Agent;
use App\Models\App;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\Settings;
use App\Models\LoginAgents;
use App\Helper\Log;
use Validator;
use Auth;
use Hash;
Use DB;


class AuthController extends Controller
{
    //
    public function index() {
        $title = 'Login';
        $settings = Settings::all();
        return view('Auth.pages.login',compact(
            'title',
            'settings'
        ));
    }

    public function register() {
        $title = 'Daftar Akun';
        $settings = Settings::all();
        return view('Auth.pages.register',compact(
            'title',
            'settings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Validator::make(
            $request->all(),
            Pelanggan::storeRule(),
            App::ValidationMessage()
        )->validate();

        if($request->pelanggan_foto != NULL){
            $foto = $request->file('pelanggan_foto');
			      $path  = 'assets/pelanggan';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = rand(100000,1001238912).".".$ext;
            $foto->move($path,$namafoto);
            $upload =  '/'.$path.'/'.$namafoto;
        } else{
            $upload   = '/assets/images/default/default.png';
        }

        $id_pelanggan = date('Ymd').rand(1,100);

        Pelanggan::create([
            'role_id' => 3,
            'pelanggan_id' => $id_pelanggan,
            'pelanggan_nama' => $request->pelanggan_nama,
            'pelanggan_alamat' => $request->pelanggan_alamat,
            'pelanggan_tanggal_lahir' => $request->pelanggan_tanggal_lahir,
            'pelanggan_foto' => $upload,
            'pelanggan_username' => $id_pelanggan,
            'pelanggan_password' => Hash::make($request->pelanggan_tanggal_lahir),
        ]);

        return redirect()->route('auth.login')->with([
            'success' => 'Pendaftaran Sukses Silahkan Login',
        ]);

    }

    public function auth(Request $request) {

        Validator::make($request->all(), [
			    'user_username' => 'required',
			    'user_userpassword' => 'required',
        ], App::ValidationMessage())->validate();

        if(Auth::guard('pelanggan')->attempt([
			    'pelanggan_username' => $request->user_username,
			    'password' => $request->user_userpassword])) {

                return redirect()->route('dashboard.index');

		    } else if(Auth::guard('petugas')->attempt([
			     'petugas_username' => $request->user_username,
			     'password' => $request->user_userpassword])) {

              $petugas = Petugas::where('petugas_nip',$request->user_username)->get();
              foreach($petugas as $pk) {
                $petugas_id = $pk->id;
              }

               $log = Log::LoginLog($petugas_id);
               if($log) {
                 return redirect()->route('dashboard.index');
              } else {
                  return redirect()->back()->withErrors(['Gagal Login System Sedang Bermasalah Coba Sekali Lagi']);
              }
        }

        return redirect()->back()->withErrors(['User tidak ditemukan']);
    }

    public function logout() {

    if(Auth::guard('petugas')->check()) {
      Log::LogoutLog(Auth::guard('petugas')->user()->id);
      Auth::guard('petugas')->logout();
      return redirect()->route('auth.login');
		}
		else if(Auth::guard('pelanggan')->check()) {
			Auth::guard('pelanggan')->logout();
			return redirect()->route('auth.login');
		}
  }
}
