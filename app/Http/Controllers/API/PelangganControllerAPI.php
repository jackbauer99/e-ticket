<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use Hash;
use App\Models\Pelanggan;
use App\Models\Role;
use App\Models\App;

class PelangganControllerAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = "Dashboard - Pelanggan";
        $pelanggan = Pelanggan::all();
        return response()->json([
            'message' => 'Data Ditampilkan',
            'pelanggan' => $pelanggan,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $title = "Dashboard - Pelanggan";
        /*return view('Dashboard.back.Pelanggan.create',compact(
            'title'
        ));*/

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make(
            $request->all(),
            Pelanggan::storeRule(), 
            App::ValidationMessage()
        );

        if($validator->fails()){
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $id_pelanggan = date('Ymd').rand(1,100);

        if($request->pelanggan_foto != NULL){
            $foto = $request->file('pelanggan_foto');
			$path  = 'assets/pelanggan';                                                                                                                          
        	$ext = $foto->getClientOriginalExtension();
            $namafoto = $id_pelanggan.".".$ext;
            $foto->move($path,$namafoto);
            $upload =  '/'.$path.'/'.$namafoto;        
        } else{
            $upload   = '/assets/images/default/default.png';  
        }  

        Pelanggan::create([
            'role_id' => 3,
            'pelanggan_id' => $id_pelanggan,
            'pelanggan_nama' => $request->pelanggan_nama,
            'pelanggan_alamat' => $request->pelanggan_alamat,
            'pelanggan_tanggal_lahir' => $request->pelanggan_tanggal_lahir,
            'pelanggan_jk' => $request->pelanggan_jk,
            'pelanggan_foto' => $upload,
            'pelanggan_username' => $id_pelanggan,
            'pelanggan_password' => Hash::make($request->pelanggan_tanggal_lahir),
        ]);

        return response()->json(['success' => 'Data Sukses Ditambahkan']);  

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pelanggan = Pelanggan::find($id);
        return response()->json([
            'messages' => 'success',
            'data' => $pelanggan,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $title = 'Dashboard - Pelanggan';
        $pelanggan = Pelanggan::where('id',$id)->get();
        //dd($pelanggan);
        return response()->json([
            'pesan' => 'Data Ditampilkan',
            'pelanggan' => $pelanggan,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = Validator::make(
            $request->all(),
            Pelanggan::storeRule(), 
            App::ValidationMessage()
        );
        
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        //$id_pelanggan = date('Ymd').rand(1,100);

        $pelanggan = Pelanggan::find($id);

        $id_pelanggan = $pelanggan->pelanggan_id;

        if($request->pelanggan_foto != NULL){
            $app_path = "/home/nugraha/WEBPROJECT/e-ticket5.4/public";
            unlink($app_path.$pelanggan->pelanggan_foto);
            $foto = $request->file('pelanggan_foto');
			$path  = 'assets/pelanggan';                                                                                                                          
        	$ext = $foto->getClientOriginalExtension();
            $namafoto = $id_pelanggan.".".$ext;
            $foto->move($path,$namafoto);
            $upload =  '/'.$path.'/'.$namafoto;        
        } else{
            $upload   = $pelanggan->pelanggan_foto;  
        }  

        $pelanggan = Pelanggan::find($id);

        $pelanggan->update([
            'role_id' => 3,
            'pelanggan_id' => $id_pelanggan,
            'pelanggan_nama' => $request->pelanggan_nama,
            'pelanggan_alamat' => $request->pelanggan_alamat,
            'pelanggan_tanggal_lahir' => $request->pelanggan_tanggal_lahir,
            'pelanggan_jk' => $request->pelanggan_jk,
            'pelanggan_foto' => $upload,
            'pelanggan_username' => $id_pelanggan,
            'pelanggan_password' => Hash::make($request->pelanggan_tanggal_lahir),
        ]);

        return response()->json(['success' => 'Data Berhasil Diubah']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $app_path = "/home/nugraha/WEBPROJECT/e-ticket5.4/public";
        $delete_pelanggan = Pelanggan::where('id',$id)->first();
        if($delete_pelanggan->foto_pelanggan != NULL){
            unlink($app_path.$delete_pelanggan->pelanggan_foto);
        }
        $delete_pelanggan->delete();
        
        return response()->json(['success' => 'Data Berhasil Dihapus']);
    }
}
