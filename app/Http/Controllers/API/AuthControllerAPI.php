<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Jenssegers\Agent\Agent;
use App\Models\App;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\TokenLogin;
use App\Models\LoginAgents;
use App\Helper\Log;
use Validator;
use Auth;
use Hash;
Use DB;

class AuthControllerAPI extends Controller
{
    //
    public function index() {
        $title = 'Login';
        return view('Auth.pages.login',compact('title'));
    }

    public function register() {
        $title = 'Daftar Akun';
        return view('Auth.pages.register',compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make(
            $request->all(),
            Pelanggan::storeRule(),
            App::ValidationMessage()
        );

        if($validator->fails()) {
            return response()->json([
                'errors' => TRUE,
                'messages' => $validator->errors()->all(),
            ]);
        }

        if($request->pelanggan_foto != NULL){
            $foto = $request->file('pelanggan_foto');
			      $path  = 'assets/pelanggan';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = rand(100000,1001238912).".".$ext;
            $foto->move($path,$namafoto);
            $upload =  '/'.$path.'/'.$namafoto;
        } else{
            $upload   = '/assets/images/default/default.png';
        }

        $id_pelanggan = date('Ymd').rand(1,100);

        Pelanggan::create([
            'role_id' => 3,
            'pelanggan_id' => $id_pelanggan,
            'pelanggan_nama' => $request->pelanggan_nama,
            'pelanggan_alamat' => $request->pelanggan_alamat,
            'pelanggan_tanggal_lahir' => $request->pelanggan_tanggal_lahir,
            'pelanggan_foto' => $upload,
            'pelanggan_username' => $id_pelanggan,
            'pelanggan_password' => Hash::make($request->pelanggan_tanggal_lahir),
        ]);

        return response()->json(['success'=>'Pendaftaran Sukses Silahkan Login']);

    }

    public function auth(Request $request) {

        $validator = Validator::make($request->all(), [
			       'user_username' => 'required',
			       'user_userpassword' => 'required',
        ], App::ValidationMessage());

        if($validator->fails()) {
            return response()->json([
                'success' => TRUE,
                'messages' => "Inputan tidak boleh kosong",
            ]);
        }

        if(Auth::guard('pelanggan')->attempt([
			       'pelanggan_username' => $request->user_username,
			       'password' => $request->user_userpassword])) {

              return response()->json([
                  'success' => 'Login As Pelanggan'
                ]);

		} else if(Auth::guard('petugas')->attempt([
			'petugas_username' => $request->user_username,
			'password' => $request->user_userpassword])
    ) {

        $all = Petugas::where('petugas_nip',$request->user_username)->first();
        $token = md5(uniqid($all['petugas_nip']));
        $expired_date = strtotime('+1 days', strtotime(date('Y-m-d')));

        $petugas = Petugas::where('petugas_nip',$request->user_username)->get();
        foreach($petugas as $pk) {
          $petugas_id = $pk->id;
        }

         Log::LoginLog($petugas_id);

        TokenLogin::create([
          'token' => $token,
          'expired_date' => date('Y-m-d',$expired_date),
        ]);

				return response()->json([
            'success' => TRUE,
            'messages' => 'Login Sebagai Petugas',
            'data' => $all,
            'token' => $token
          ]);
        }

        return response()->json([
            'success' => TRUE,
            'messages' => ' User Tidak Ditemukan '
        ]);
    }

    public function logout() {

        if(Auth::guard('petugas')->check()) {
			Auth::guard('petugas')->logout();
			return response()->json([
                'success' => 'Petugas Sudah Logout'
            ]);
		}
		else if(Auth::guard('pelanggan')->check())
		{
			Auth::guard('pelanggan')->logout();
			return response()->json([
                'success' => 'Pelanggan Sudah Logout'
            ]);
		}
    }
}
