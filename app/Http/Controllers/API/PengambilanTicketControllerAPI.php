<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use DB;
use App\Models\Ticket;
use App\Models\Realisasi;
use App\Models\TokenLogin;

class PengambilanTicketControllerAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Dashboard - Ambil Tiket';
        return view('Dashboard.back.Ticket.ambil',compact(
            'title'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id,$request)
    {
        //

        // Cek Token Login API Dulu
        $token = TokenLogin::where('token',$request)->first();

        if(empty($token)) {
          return response()->json([
            'messages' => 'Token Not Found',
          ]);
        } else if(!empty($token)){
          if($token['expired_date'] < date('Y-m-d')) {
            return response()->json([
              'messages' => 'Token Expired',
            ]);
          }
        }

        $realisasi = Realisasi::all();

        $ticket = Ticket::where('is_diambil',0)->first();
        $hitung = Ticket::where('is_diambil',0)->count();

        //
        foreach($realisasi as $r) {
            if($id == $r['petugas_id'] && $r['is_realisasi'] == 0) {
                return response()->json([
                    'success' => TRUE,
                    'status' => 'failed',
                    'messages' => 'Nomor Sudah Diambil'
                ]);
            }
        }
        if($hitung > 0) {
            $randomize = $ticket['id'];

            $uptic = Ticket::find($randomize);

            $uptic->update([
                'tiket_diambil' => date('Ymd'),
                'is_diambil' => 1,
            ]);

            Realisasi::create([
                'ticket_id' => $randomize,
                'petugas_id' => $id,
                'is_realisasi' => 0,
            ]);

            return response()->json([
                'success' => TRUE,
                'status' => 'success',
                'messages' => 'Tiket Sukses Diambil',
                'data' => 'No Ticket Anda '.$uptic->no_ticket
            ]);
        } else {
            return response()->json([
                'success' => TRUE,
                'status' => 'warning',
                'messages' => 'Tiket Sudah Habis'
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
