<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use Hash;
use DB;
use App\Models\Pelanggan;
use App\Models\Role;
use App\Models\App;
use App\Models\TokenLogin;
use App\Models\Keluhan;
use App\Models\Ticket;


class KeluhanControllerAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($request)
    {
        //
        $title = "Dashboard - Keluhan";
        $keluhan = DB::table('pelanggan')
                ->join('keluhan','pelanggan.id','=','keluhan.pelanggan_id')
                ->get();

        $token = TokenLogin::where('token',$request)->first();
        // Cek Token API Dulu
        if($request == !empty($token['token']) && !empty($token['expired_date']) <= date('Y-m-d')) {
          return response()->json([
              'messages' => 'Data Berhasil Tampil',
              'data' => $keluhan,
          ]);
        } else if(empty($token) || !empty($token['expired_date']) > date('Y-m-d')){
            return response()->json([
              'messages' => 'Token Expired',
          ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $title = "Keluhan Pelanggan";
        $pelanggan = Pelanggan::all();
        return view('Dashboard.back.Keluhan.create',compact(
            'title',
            'pelanggan'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $tgl = date('Ymd');

        $generate_ticket = 'T-'.rand(1,100).'-'.$tgl;

        $ticket = Ticket::all();
        $hitung = Ticket::all()->count();


        if($hitung == 0) {

            Ticket::create([
                'no_ticket' => $generate_ticket,
                'tiket_tanggal' => $tgl,
                'is_diambil' => 0
            ]);

        } else {
            foreach($ticket as $tc) {
                $t = $tc['no_ticket'];
            }

            if($t != $generate_ticket) {
                Ticket::create([
                    'no_ticket' => $generate_ticket,
                    'tiket_tanggal' => $tgl,
                    'is_diambil' => 0
                ]);
            }
        }

        $tiket = Ticket::all();

        foreach($tiket as $hk) {
            $k = $hk['id'];
        }

        Keluhan::create([
            'pelanggan_id' => $request->pelanggan_nama,
            'ticket_id' => $k,
            'keluhan_nama' => $request->keluhan_nama,
            'keluhan_tanggal' => $tgl,
        ]);

        return response()->json([
            'messages' => 'Data Berhasil Disimpan',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete_keluhan = Keluhan::where('id',$id)->first();
        $delete_keluhan->delete();
        return response()->json([
            'messages' => 'Data Berhasil Dihapus',
        ]);
    }
}
