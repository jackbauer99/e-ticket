<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

use App\Models\Ticket;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\Keluhan;
use App\Models\Realisasi;
use App\Models\TokenLogin;

class TicketControllerAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($request)
    {
        //
        $title = "Dashboard - Tiket";
        $ticket = Ticket::all();
        $token = TokenLogin::where('token',$request)->first();
        // Cek Token API Dulu
        if($request == !empty($token['token']) && !empty($token['expired_date']) <= date('Y-m-d')) {
          return response()->json([
              'messages' => 'Data Berhasil Tampil',
              'data' => $ticket,
          ]);
        } else if(empty($token)) {
          return response()->json([
              'messages' => 'Token Expired',
          ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Delete Realisasi Dulu (Foreign Key)
        $app_path = "/home/nugraha/WEBPROJECT/e-ticket5.4/public";
        $delete_realisasi = Realisasi::where('ticket_id',$id)->first();
        if($delete_realisasi->foto_realiasasi_1 != NULL) {
            unlink($app_path.$delete_realisasi->foto_realiasasi_1);
        } else if($delete_realisasi->foto_realiasasi_2 != NULL) {
            unlink($app_path.$delete_realisasi->foto_realiasasi_2);
        } else if($delete_realisasi->foto_realiasasi_3 != NULL) {
            unlink($app_path.$delete_realisasi->foto_realiasasi_3);
        } else if($delete_realisasi->foto_realiasasi_4 != NULL) {
            unlink($app_path.$delete_realisasi->foto_realiasasi_4);
        }
        $delete_realisasi->delete();

        // Delete Keluhan Dulu (Foreign Key)
        $delete_keluhan = Keluhan::where('ticket_id',$id)->delete();

        // Baru Delete Tiket Biar Tidak Error
        $delete = Ticket::where('id',$id)->first();
        $delete->delete();

        return response()->json([
            'massages' => 'Data Berhasil Dihapus'
        ]);
    }
}
