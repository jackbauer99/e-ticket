<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

use App\Models\Realisasi;
use App\Models\Ticket;
use App\Models\Petugas;
use App\Models\Keluhan;
use App\Models\Pelanggan;
use App\Models\TokenLogin;

class RealisasiControllerAPI extends Controller
{
    //
    public function index($id,$request) {

        //
        $title = "Dashboard - Realisasi";

        // Cek Token Login API Dulu
        $token = TokenLogin::where('token',$request)->first();

        if(empty($token)) {
          return response()->json([
            'messages' => 'Token Not Found',
          ]);
        } else if(!empty($token)){
          if($token['expired_date'] < date('Y-m-d')) {
            return response()->json([
              'messages' => 'Token Expired',
            ]);
          }
        }

        $realisasi = Realisasi::where(['petugas_id' => $id, 'is_realisasi' => 0])->get();
        $realisasi_count = Realisasi::where('petugas_id',$id)->count();
        $petugas = Petugas::all();
        $ticket = Ticket::all();
        $keluhan = Keluhan::all();
        foreach($realisasi as $hasil) {
          $realisasi1[] = array(
            'id' => $hasil['id'],
            'no_ticket' => $hasil->ticket['no_ticket'],
            'nama_petugas' => $hasil->petugas['petugas_nama'],
            'keluhan' => $hasil->keluhan['keluhan_nama'],
            'nama_pelanggan' => $hasil->keluhan->pelanggan['pelanggan_nama'],
            'alamat_pelanggan' => $hasil->keluhan->pelanggan['pelanggan_alamat'],
            'realisasi_nama' => $hasil['realisasi_nama'],
            'foto_realisasi_1' => $hasil['foto_realisasi_1'],
            'foto_realisasi_2' => $hasil['foto_realisasi_2'],
            'foto_realisasi_3' => $hasil['foto_realisasi_3'],
            'foto_realisasi_4' => $hasil['foto_realisasi_4'],
            'realisasi' => $hasil['is_realisasi'],
            'realisasi_tanggal' => $hasil['realisasi_tanggal'],
          );
        }
        if($realisasi_count < 1 || empty($realisasi1)) {
          $response = array(
            'status' => FALSE,
            'messages' => 'Data Kosong',
            'hitung' => 0,
          );
        } else if($realisasi_count > 0) {
          $response =  array(
            'status' => TRUE,
            'messages' => 'Data Sukses Ditampilkan',
            'hitung' => 1,
            'realisasi' => $realisasi1,
          );
        }
        return response()->json($response);
    }

    public function update(Request $request, $id, $cek) {

        //

        // Cek Token Login API Dulu
        $token = TokenLogin::where('token',$cek)->first();

        if(empty($token)) {
          return response()->json([
            'messages' => 'Token Not Found',
          ]);
        } else if(!empty($token)){
          if($token['expired_date'] < date('Y-m-d')) {
            return response()->json([
              'messages' => 'Token Expired',
            ]);
          }
        }

        // Ambil nomor tiket dulu
        $notiket = Realisasi::where('id',$id)->get();
        foreach($notiket as $no) {
            $no_tiket = $no->ticket_id;
        }

        // Ambil id tiket dari nomor tiket yang sudah diambil
        $tiket = Ticket::where('id',$no_tiket)->first();

        // Update realisasi diambil dari id ticket (Foreign Key)
        $update_realisasi = Realisasi::where('id',$id)->first();

        // Beri Pesan Error Jika Realisasi Sudah Terverifikasi
        if($update_realisasi['is_realisasi'] == 2) {
          return response()->json([
            'status' => FALSE,
            'messages' => 'Data Sudah Terverifikasi Jadi Tidak Bisa Diubah Lagi Silahkan Hubungi Petugas Kantor',
            'hitung' => 3,
          ]);
        }

        // Upload Foto Realisasi 1
        if($request->realisasi_foto_1 != NULL){
            $foto = $request->file('realisasi_foto_1');
			      $path  = 'public/assets/realisasi';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = $tiket->no_ticket."1".".".$ext;
            $foto->move($path,$namafoto);
            $upload1 =  $path.'/'.$namafoto;
        } else{
            $upload1   = NULL;
        }

        // Upload Foto Realisasi 2
        if($request->realisasi_foto_2 != NULL){
            $foto = $request->file('realisasi_foto_2');
			      $path  = 'public/assets/realisasi';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = $tiket->no_ticket."2".".".$ext;
            $foto->move($path,$namafoto);
            $upload2 =  $path.'/'.$namafoto;
        } else{
            $upload2   = NULL;
        }

        // Upload Foto Realisasi 3
        if($request->realisasi_foto_3 != NULL){
            $foto = $request->file('realisasi_foto_3');
			      $path  = 'public/assets/realisasi';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = $tiket->no_ticket."3".".".$ext;
            $foto->move($path,$namafoto);
            $upload3 =  $path.'/'.$namafoto;
        } else{
            $upload3  = NULL;
        }

        // Upload Foto Realisasi 4
        if($request->realisasi_foto_4 != NULL){
            $foto = $request->file('realisasi_foto_4');
			      $path  = 'public/assets/realisasi';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = $tiket->no_ticket."4".".".$ext;
            $foto->move($path,$namafoto);
            $upload4 =  $path.'/'.$namafoto;
        } else{
            $upload4   = NULL;
        }

        // Update Realisasinya
        $update_realisasi->update([
            'realisasi_nama' => $request->realisasi_nama,
            'foto_realisasi_1' => $upload1,
            'foto_realisasi_2' => $upload2,
            'foto_realisasi_3' => $upload3,
            'foto_realisasi_4' => $upload4,
            'realisasi_tanggal' => date('Ymd'),
            'is_realisasi' => 1,
        ]);

        // Update Tiket Juga untuk menandakan bahwa tiket sudah terealisasikan
        $tiket->update([
            'tiket_tanggal_realisasi' => date('Ymd'),
        ]);

        return response()->json([
            'status' => true,
            'messages' => 'Data Sukses Dikirim',
        ]);
    }
}
