<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\Ticket;
use App\Models\Keluhan;
use App\Models\Settings;
use App\Models\LoginAgents;

class DashboardController extends Controller
{
    //
    public function index() {
        $title = 'Dashboard';
        $agent = new Agent();
        $settings = Settings::all();
        $pelanggan = Pelanggan::all()->count();
        $petugas = Petugas::all()->count();
        $keluhan = Keluhan::all()->count();
        $ticket = Ticket::where('is_diambil',0)->count();
        return view('Dashboard.pages.dashboard',compact(
            'title',
            'pelanggan',
            'petugas',
            'keluhan',
            'ticket',
            'settings'
        ));
    }

    public function getSettingsLayout() {

        $settings = Settings::where('id',1)->get();
        $title = 'Dashboard - Pengaturan';
        return view('Dashboard.pages.settings',compact(
            'settings',
            'title',
        ));
    }

    public function setSettings(Request $request, $id) {

        $dashboard = Settings::find($id);

        if($request->login_image != NULL){
            if($dashboard->login_image != NULL) {
                //chmod($dashboard->login_image, 0777);
                unlink($dashboard->login_image);
            }
            $foto = $request->file('login_image');
			      $path  = 'public/assets/settings';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = "login".".".$ext;
            $foto->move($path,$namafoto);
            $upload1 =  $path.'/'.$namafoto;
        } else{
            $upload1   = $dashboard->login_image;
        }

        if($request->register_image != NULL){
            if($dashboard->register_image != NULL) {
                chmod($dashboard->register_image, 0777);
                unlink($dashboard->register_image);
            }
            $foto = $request->file('register_image');
			      $path  = 'public/assets/settings';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = "register".".".$ext;
            $foto->move($path,$namafoto);
            $upload2 =  $path.'/'.$namafoto;
        } else{
            $upload2   = $dashboard->register_image;
        }

        $dashboard->update([
            'dashboard_name' => $request->dashboard_name,
            'login_image' => $upload1,
            'register_image' => $upload2,
        ]);

        return redirect()->route('dashboard.index')->with([
            'success' => 'Dashboard Sukses Diubah',
        ]);
    }

    public function setLoginActivity() {
      $loginactivity = LoginAgents::all();
      $settings = Settings::where('id',1)->get();
      $title = 'Dashboard - Aktivitas Login';
      return view ('Dashboard.pages.loginactivity',compact(
        'title',
        'settings',
        'loginactivity',
      ));
    }
}
