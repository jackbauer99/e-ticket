<?php
namespace App\Http\Controllers\Master;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;
use Auth;

use App\Helper\Log;
use App\Models\Realisasi;
use App\Models\Ticket;
use App\Models\Petugas;
use App\Models\Keluhan;
use App\Models\Pelanggan;
use App\Models\Settings;

class RealisasiController extends Controller
{
    //

    public function index() {

        //
        $title = "Dashboard - Realisasi";
        $settings = Settings::all();

        // Cek Auth petugas dulu
        if(Auth::guard('petugas')->check()) {

            // Jika Auth nya petugas dengan jabatan petugas kantor dan admin
            if(Auth::guard('petugas')->user()->role_id == 1 || Auth::guard('petugas')->user()->role_id == 2){
                $realisasi = Realisasi::all();
                $petugas = Petugas::all();
                $ticket = Ticket::all();
            }
            // Jika selain Jabatan petugas kantor dan admin
            else {
                $realisasi = Realisasi::where('petugas_id',Auth::guard('petugas')->user()->id)->get();
                $petugas = Petugas::all();
                $ticket = Ticket::all();
            }
        }
        return view('Dashboard.back.Realisasi.realisasi',compact(
            'title',
            'realisasi',
            'petugas',
            'ticket',
            'settings'
        ));
    }

    public function edit($id) {
        //
        $title = "Dashboard - Realisasi";
        $settings = Settings::all();
        $realisasi = Realisasi::where('id',$id)->get();
        $petugas = Petugas::all();
        $ticket = Ticket::all();
        $keluhan = Keluhan::all();
        $pelanggan = Pelanggan::all();
        return view('Dashboard.back.Realisasi.edit',compact(
            'title',
            'realisasi',
            'petugas',
            'ticket',
            'pelanggan',
            'keluhan',
            'settings'
        ));
    }

    public function editAct(Request $request, $id) {

        // Ambil nomor tiket dulu
        $notiket = Realisasi::where('id',$id)->get();
        foreach($notiket as $no) {
            $no_tiket = $no->ticket_id;
        }

        // Ambil id tiket dari nomor tiket yang sudah diambil
        $tiket = Ticket::where('id',$no_tiket)->first();

        // Update realisasi diambil dari id ticket (Foreign Key)
        $update_realisasi = Realisasi::where('id',$id)->first();

        // Upload Foto Realisasi 1
        if($request->realisasi_foto_1 != NULL){
            $foto = $request->file('realisasi_foto_1');
			      $path  = 'public/assets/realisasi';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = $tiket->no_ticket."1".".".$ext;
            $foto->move($path,$namafoto);
            $upload1 =  $path.'/'.$namafoto;
        } else{
            $upload1   = NULL;
        }

        // Upload Foto Realisasi 2
        if($request->realisasi_foto_2 != NULL){
            $foto = $request->file('realisasi_foto_2');
			      $path  = 'public/assets/realisasi';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = $tiket->no_ticket."2".".".$ext;
            $foto->move($path,$namafoto);
            $upload2 =  $path.'/'.$namafoto;
        } else{
            $upload2   = NULL;
        }

        // Upload Foto Realisasi 3
        if($request->realisasi_foto_3 != NULL){
            $foto = $request->file('realisasi_foto_3');
			      $path  = 'public/assets/realisasi';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = $tiket->no_ticket."3".".".$ext;
            $foto->move($path,$namafoto);
            $upload3 =  $path.'/'.$namafoto;
        } else{
            $upload3  = NULL;
        }

        // Upload Foto Realisasi 4
        if($request->realisasi_foto_4 != NULL){
            $foto = $request->file('realisasi_foto_4');
			      $path  = 'public/assets/realisasi';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = $tiket->no_ticket."4".".".$ext;
            $foto->move($path,$namafoto);
            $upload4 =  $path.'/'.$namafoto;
        } else{
            $upload4   = NULL;
        }

        // Update Realisasinya
        $update_realisasi->update([
            'realisasi_nama' => $request->realisasi_nama,
            'foto_realisasi_1' => $upload1,
            'foto_realisasi_2' => $upload2,
            'foto_realisasi_3' => $upload3,
            'foto_realisasi_4' => $upload4,
            'realisasi_tanggal' => date('Ymd'),
            'is_realisasi' => 1,
        ]);

        // Update Tiket Juga untuk menandakan bahwa tiket sudah terealisasikan
        $tiket->update([
            'tiket_tanggal_realisasi' => date('Ymd'),
        ]);
        Log::InsertLogAct(Auth::guard('petugas')->user()->id,'Realisasi');
        return redirect()->route('realisasi.index')->with([
            'success' => 'Realisasi Sudah diselesaikan',
        ]);

    }

    public function destroy($id) {

        if(Auth::guard('petugas')->user()->role_id == 1 || Auth::guard('petugas')->user()->role_id == 2){
            // Update Data Tiket Dulu biar nanti bisa diambil lagi tiketnya
            $cek_realisasi = Realisasi::where('id',$id)->first();
            $update_tiket = Ticket::where('id',$cek_realisasi->ticket_id)->first();
            $update_tiket->update([
                'tiket_tanggal_realisasi' => NULL,
                'tiket_diambil' => NULL,
                'is_diambil' => 0,
            ]);

            // Baru Hapus Realisasi nya
            $delete_realisasi = Realisasi::where('id',$id)->first();
            if($delete_realisasi->foto_realiasasi_1 != NULL) {
                unlink($delete_realisasi->foto_realiasasi_1);
            } if($delete_realisasi->foto_realiasasi_2 != NULL) {
                unlink($delete_realisasi->foto_realiasasi_2);
            } if($delete_realisasi->foto_realiasasi_3 != NULL) {
                unlink($delete_realisasi->foto_realiasasi_3);
            } if($delete_realisasi->foto_realiasasi_4 != NULL) {
                unlink($delete_realisasi->foto_realiasasi_4);
            }
            $delete_realisasi->delete();
            Log::DeleteLogAct(Auth::guard('petugas')->user()->id,'Realisasi');
            return redirect()->route('realisasi.index')->with([
                'success' => 'Data Berhasil Dihapus',
            ]);
        }
    }

    public function upVerification($id) {
        if(Auth::guard('petugas')->user()->role_id == 1 || Auth::guard('petugas')->user()->role_id == 2){
            $realisasi = Realisasi::find($id);
            if($realisasi->is_realisasi == 1) {
                $realisasi->update([
                    'is_realisasi' => 2,
                ]);
            }
        }
        Log::verification(Auth::guard('petugas')->user()->id,'up');
        return redirect()->route('realisasi.index')->with([
            'success' => 'Realisasi Telah Disetujui',
        ]);
    }

    public function downVerification($id) {
        if(Auth::guard('petugas')->user()->role_id == 1 || Auth::guard('petugas')->user()->role_id == 2){
            $realisasi = Realisasi::find($id);
            if($realisasi->is_realisasi == 2) {
                $realisasi->update([
                    'is_realisasi' => 1,
                ]);
            }
        }
        Log::verification(Auth::guard('petugas')->user()->id,'down');
        return redirect()->route('realisasi.index')->with([
            'success' => 'Realisasi Telah Dibatalkan',
        ]);
    }
}
