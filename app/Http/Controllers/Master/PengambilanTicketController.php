<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use DB;
use App\Models\Ticket;
use App\Models\Realisasi;
use App\Models\Settings;

class PengambilanTicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = 'Dashboard - Ambil Tiket';
        $settings = Settings::all();
        return view('Dashboard.back.Ticket.ambil',compact(
            'title',
            'settings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Cek auth apakah guard nya petugas
        if(Auth::guard('petugas')->check()) {
            $id = Auth::guard('petugas')->user()->id;
        }

        // Tampilkan semua realisasi
        $realisasi = Realisasi::all();

        // Tampilkan tiket yang is_diambil nya masih 0
        $ticket = Ticket::where('is_diambil',0)->first();
        // Hitung tiket yang is_diambil nya masih 0
        $hitung = Ticket::where('is_diambil',0)->count();

        // Cek jika realisasi sudah ada petugas yang sama mengambil tiket 
        foreach($realisasi as $r) {
            if($id == $r['petugas_id'] && $r['is_realisasi'] == 0) {
                return redirect()->route('ticketact.index')->with([
                    'failed' => ' Anda Telah Mengambil Tiket silahkan untuk merealisasikan terlebih dahulu ',
                ]);
            }
        }

        // Cek jika tiket belum ada yang mengambil
        if($hitung > 0) {
            $randomize = $ticket['id'];

            $uptic = Ticket::find($randomize);

            $uptic->update([
                'tiket_diambil' => date('Ymd'),
                'is_diambil' => 1,
            ]);
            
            // Update realisasi setelah pengambilan tiket
            Realisasi::create([
                'ticket_id' => $randomize,
                'petugas_id' => $id,
                'is_realisasi' => 0,
            ]);

            return redirect()->route('ticketact.index')->with([
                'success' => ' NO Tiket Anda '.$uptic->no_ticket,
            ]);
        } 
        // Jika tiket sudah habis
        else {

            return redirect()->route('ticketact.index')->with([
                'warning' => ' NO Tiket Telah Habis ',
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
