<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

use App\Models\Ticket;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\Keluhan;
use App\Models\Realisasi;
use App\Models\Settings;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = "Dashboard - Tiket";
        $ticket = Ticket::all();
        $settings = Settings::all();
        return view('Dashboard.back.Ticket.ticket',compact(
            'title',
            'ticket',
            'settings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Delete Realisasi Dulu (Foreign Key)
        $hitung_realisasi = Realisasi::all()->count();
        if($hitung_realisasi > 0) {
            $realisasi_all = Realisasi::all();
            foreach($realisasi_all as $rel) {
                $id_ticket = $rel['ticket_id'];
            }
            if($id == $id_ticket) {
                $hitung_realisasi_by_id = Realisasi::where('ticket_id',$id)->count();
                if($hitung_realisasi_by_id > 0) {
                    $delete_realisasi = Realisasi::where('ticket_id',$id)->first();
                    if($delete_realisasi->foto_realiasasi_1 != NULL) {
                        unlink($delete_realisasi->foto_realiasasi_1);
                    } else if($delete_realisasi->foto_realiasasi_2 != NULL) {
                        unlink($delete_realisasi->foto_realiasasi_2);
                    } else if($delete_realisasi->foto_realiasasi_3 != NULL) {
                        unlink($delete_realisasi->foto_realiasasi_3);
                    } else if($delete_realisasi->foto_realiasasi_4 != NULL) {
                        unlink($delete_realisasi->foto_realiasasi_4);
                    }
                    $delete_realisasi->delete();
                }
            }
        }

        // Delete Keluhan Dulu (Foreign Key)
        $hitung_keluhan = Keluhan::all()->count();
        if($hitung_keluhan > 0) {
            $keluhan_all = Keluhan::all();
            foreach($keluhan_all as $kel) {
                $id_ticket = $kel['ticket_id'];
            }
            if($id == $id_ticket) {
                $hitung_keluhan_by_id = Keluhan::where('ticket_id',$id)->count();
                if($hitung_keluhan_by_id > 0) {
                    $delete_keluhan = Keluhan::where('ticket_id',$id)->first();
                    $delete_keluhan->delete();
                }
            }
        }

        // Baru Delete Tiket Biar Tidak Error
        $delete = Ticket::where('id',$id)->first();
        $delete->delete();

        return redirect()->route('ticket.index')->with([
            'success' => 'Data Berhasil Dihapus',
        ]);
    }
}
