<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use Hash;
use DB;
use App\Helper\Log;
use App\Models\Pelanggan;
use App\Models\Keluhan;
use App\Models\Role;
use App\Models\Settings;
use App\Models\App;

class PelangganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = "Dashboard - Pelanggan";
        $settings = Settings::all();
        $pelanggan = Pelanggan::all();
        return view('Dashboard.back.Pelanggan.pelanggan',compact(
            'title',
            'pelanggan',
            'settings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $title = "Dashboard - Pelanggan";
        $settings = Settings::all();
        return view('Dashboard.back.Pelanggan.create',compact(
            'title',
            'settings',
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Validator::make(
            $request->all(),
            Pelanggan::storeRule(),
            App::ValidationMessage()
        )->validate();

        $id_pelanggan = date('Ymd').rand(1,100);

        if($request->pelanggan_foto != NULL){
            $foto = $request->file('pelanggan_foto');
			      $path  = 'public/assets/pelanggan';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = $id_pelanggan.".".$ext;
            $foto->move($path,$namafoto);
            $upload =  $path.'/'.$namafoto;
        } else{
            $upload   = '/assets/images/default/default.png';
        }

        $insert = Pelanggan::create([
            'role_id' => 3,
            'pelanggan_id' => $id_pelanggan,
            'pelanggan_nama' => $request->pelanggan_nama,
            'pelanggan_alamat' => $request->pelanggan_alamat,
            'pelanggan_tanggal_lahir' => $request->pelanggan_tanggal_lahir,
            'pelanggan_jk' => $request->pelanggan_jk,
            'pelanggan_telepon' => $request->pelanggan_telepon,
            'pelanggan_foto' => $upload,
            'pelanggan_username' => $id_pelanggan,
            'pelanggan_password' => Hash::make($request->pelanggan_tanggal_lahir),
        ]);

        if($insert) {
          Log::InsertLogAct(Auth::guard('petugas')->user()->id,'Pelanggan');
          return redirect()->route('pelanggan.index')->with([
              'success' => 'Penambahan Data Sukses',
          ]);
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $title = 'Dashboard - Pelanggan';
        $settings = Settings::all();
        $pelanggan = Pelanggan::where('id',$id)->get();
        return view('Dashboard.back.Pelanggan.edit',compact(
            'title',
            'pelanggan',
            'settings'
        ));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Validator::make(
            $request->all(),
            Pelanggan::storeRule(),
            App::ValidationMessage()
        )->validate();

        //$id_pelanggan = date('Ymd').rand(1,100);

        $pelanggan = Pelanggan::find($id);

        $id_pelanggan = $pelanggan->pelanggan_id;

        if($request->pelanggan_foto != NULL){
            if($pelanggan->pelanggan_foto != NULL) {
                unlink($pelanggan->pelanggan_foto);
            }
            $foto = $request->file('pelanggan_foto');
			      $path  = 'public/assets/pelanggan';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = $id_pelanggan.".".$ext;
            $foto->move($path,$namafoto);
            $upload =  $path.'/'.$namafoto;
        } else{
            $upload   = $pelanggan->pelanggan_foto;
        }

        $pelanggan = Pelanggan::find($id);

        $update = $pelanggan->update([
            'role_id' => 3,
            'pelanggan_id' => $id_pelanggan,
            'pelanggan_nama' => $request->pelanggan_nama,
            'pelanggan_alamat' => $request->pelanggan_alamat,
            'pelanggan_tanggal_lahir' => $request->pelanggan_tanggal_lahir,
            'pelanggan_jk' => $request->pelanggan_jk,
            'pelanggan_telepon' => $request->pelanggan_telepon,
            'pelanggan_foto' => $upload,
            'pelanggan_username' => $id_pelanggan,
            'pelanggan_password' => Hash::make($request->pelanggan_tanggal_lahir),
        ]);

        if($update) {
          Log::UpdateLogAct(Auth::guard('petugas')->user()->id,'Pelanggan');
          return redirect()->route('pelanggan.index')->with([
              'success' =>  'Data Sukses Diubah',
          ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete_keluhan = Keluhan::where('pelanggan_id',$id)->delete();
        $delete_pelanggan = Pelanggan::where('id',$id)->first();
        if($delete_pelanggan->foto_pelanggan != NULL){
            unlink($delete_pelanggan->foto_pelanggan);
        }
        $delete_pelanggan->delete();
        Log::DeleteLogAct(Auth::guard('petugas')->user()->id,'Pelanggan');
        return redirect()->route('pelanggan.index')->with([
            'success' => 'Data Berhasil Dihapus',
        ]);
    }
}
