<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use Hash;
use App\Helper\Log;
use App\Models\Petugas;
use App\Models\Role;
use App\Models\App;
use App\Models\Settings;
use App\Models\Keluhan;
use App\Models\Realisasi;

class PetugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = "Admin Dashboard - List Pegawai";
        $petugas = Petugas::all();
        $settings = Settings::all();
		    return view('Dashboard.back.Petugas.petugas',compact(
            'title',
            'petugas',
            'settings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $title = "Dashboard - Petugas";
        $settings = Settings::all();
        return view('Dashboard.back.Petugas.create',compact(
            'title',
            'settings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Validator::make(
            $request->all(),
            Petugas::storeRule(),
            App::ValidationMessage()
        )->validate();

        if($request->petugas_jabatan == 1) {
            $jabatan = "Admin";
            $nip_petugas = "SA".date('Ymd').rand(1,100);
        } else if($request->petugas_jabatan == 2) {
            $jabatan = "Petugas Kantor";
            $nip_petugas = "PK".date('Ymd').rand(1,100);
        } else if($request->petugas_jabatan == 3) {
            $jabatan = "Petugas Lapangan";
            $nip_petugas = "PL".date('Ymd').rand(1,100);
        }

        if($request->petugas_foto != NULL){
            $foto = $request->file('petugas_foto');
			      $path  = 'public/assets/petugas';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = $nip_petugas.".".$ext;
            $foto->move($path,$namafoto);
            $upload = $path.'/'.$namafoto;
        } else{
            $upload   = NULL;
        }

        $insert = Petugas::create([
            'role_id' => $request->petugas_jabatan,
            'petugas_nip' => $nip_petugas,
            'petugas_nama' => $request->petugas_nama,
            'petugas_alamat' => $request->petugas_alamat,
            'petugas_tanggal_lahir' => $request->petugas_tanggal_lahir,
            'petugas_jk' => $request->petugas_jk,
            'petugas_jabatan' => $jabatan,
            'petugas_foto' => $upload,
            'petugas_username' => $nip_petugas,
            'petugas_password' => Hash::make($request->petugas_tanggal_lahir),
            'is_diganti' => 0,
        ]);

        if($insert) {
          Log::InsertLogAct(Auth::guard('petugas')->user()->id,'Petugas');
            return redirect()->route('petugas.index')->with([
              'success' => 'Penambahan Data Sukses',
          ]);
        } else {
          return redirect()->back()->withErrors(['Penambahan Data Petugas Gagal']);
        }

        // return redirect()->route('petugas.index')->with([
        //     'success' => 'Penambahan Data Sukses',
        // ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $title = "Dashboard - Petugas";
        $settings = Settings::all();
        $petugas = Petugas::where('id',$id)->get();
        return view('Dashboard.back.Petugas.edit',compact(
            'title',
            'petugas',
            'settings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Validator::make(
            $request->all(),
            Petugas::storeRule(),
            App::ValidationMessage()
        )->validate();

        $petugas = Petugas::find($id);

        $nip_petugas = $petugas->petugas_nip;

        if($request->petugas_foto != NULL){
            if($petugas->petugas_foto != NULL) {
                // $pecah_link = explode("/",$delete_petugas->petugas_foto);
                // $link = $pecah_link[1]."/".$pecah_link[2]."/".$pecah_link[3]."/".$pecah_link[4];
                unlink($petugas->petugas_foto);
            }
            $foto = $request->file('petugas_foto');
			      $path  = 'public/assets/petugas';
        	  $ext = $foto->getClientOriginalExtension();
            $namafoto = $nip_petugas.".".$ext;
            $foto->move($path,$namafoto);
            $upload =  $path.'/'.$namafoto;
        } else{
            $upload   = $petugas->petugas_foto;
        }

        if($request->petugas_jabatan == 1) {
            $jabatan = "Admin";
        } else if($request->petugas_jabatan == 2) {
            $jabatan = "Petugas Kantor";
        } else if($request->petugas_jabatan == 3) {
            $jabatan = "Petugas Lapangan";
        }

        $update = $petugas->update([
            'role_id' => $request->petugas_jabatan,
            'petugas_nip' => $nip_petugas,
            'petugas_nama' => $request->petugas_nama,
            'petugas_alamat' => $request->petugas_alamat,
            'petugas_tanggal_lahir' => $request->petugas_tanggal_lahir,
            'petugas_jk' => $request->petugas_jk,
            'petugas_jabatan' => $jabatan,
            'petugas_foto' => $upload,
            'petugas_username' => $nip_petugas,
            'petugas_password' => $petugas->petugas_password,
            'is_diganti' => 0,
        ]);

        if($update) {
          Log::UpdateLogAct(Auth::guard('petugas')->user()->id,'Petugas');
            return redirect()->route('petugas.index')->with([
              'success' => 'Update Data Sukses',
          ]);
        } else {
          return redirect()->back()->withErrors(['Penambahan Data Petugas Gagal']);
        }

        // return redirect()->route('petugas.index')->with([
        //     'success' => 'Data Sukses Diubah',
        // ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Delete Keluhan Dulu (Foreign Key) Khusus Petugas Kantor
        $hitung_keluhan = Keluhan::all()->count();
        if($hitung_keluhan > 0) {
            $keluhan_all = Keluhan::all();
            foreach($keluhan_all as $kel) {
                $id_pet = $kel['petugas_id'];
            }
            if($id == $id_pet) {
                $hitung_keluhan_by_id = Keluhan::where('petugas_id',$id)->count();
                if($hitung_keluhan_by_id > 0) {
                    $delete_keluhan = Keluhan::where('petugas_id',$id)->first();
                    $delete_keluhan->delete();
                }
            }
        }

        // Delete Realisasi (Foreign Key) Khusus Petugas Lapangan
        $hitung_realisasi = Realisasi::all()->count();
        if($hitung_realisasi > 0) {
            $realisasi_all = Realisasi::all();
            foreach($realisasi_all as $rel) {
                $id_pet = $rel['petugas_id'];
            }
            if($id == $id_pet) {
                $hitung_realisasi_by_id = Realisasi::where('petugas_id',$id)->count();
                if($hitung_realisasi_by_id > 0) {
                    $delete_realisasi = Realisasi::where('petugas_id',$id)->first();
                    if($delete_realisasi->foto_realiasasi_1 != NULL) {
                        unlink($delete_realisasi->foto_realiasasi_1);
                    } else if($delete_realisasi->foto_realiasasi_2 != NULL) {
                        unlink($delete_realisasi->foto_realiasasi_2);
                    } else if($delete_realisasi->foto_realiasasi_3 != NULL) {
                        unlink($delete_realisasi->foto_realiasasi_3);
                    } else if($delete_realisasi->foto_realiasasi_4 != NULL) {
                        unlink($delete_realisasi->foto_realiasasi_4);
                    }
                    $delete_realisasi->delete();
                }
            }
        }

        // Jika tidak ada nilai value pada Foreign Key otomatis akan menghapus Data Petugas
        $delete_petugas = Petugas::where('id',$id)->first();
        if($delete_petugas->petugas_foto != NULL){
            unlink($delete_petugas->petugas_foto);
        }
        $delete_petugas->delete();
        Log::DeleteLogAct(Auth::guard('petugas')->user()->id,'Petugas');
        return redirect()->route('petugas.index')->with([
            'success' => 'Data Berhasil Dihapus',
        ]);
    }

    public function changePass($id) {
        $title = "Dashboard - Petugas";
        $settings = Settings::all();
        $petugas = Petugas::where('id',$id)->get();
        return view('Dashboard.back.Petugas.password',compact(
            'title',
            'petugas',
            'settings'
        ));
    }

    public function password(Request $request, $id) {
        $petugas = Petugas::find($id);
        if($petugas->petugas_tanggal_lahir == $request->old_password) {
            if($request->new_password == $request->new_password_repeat) {
                $petugas->update([
                    'petugas_password' => Hash::make($request->new_password),
                    'is_diganti' => 1,
                ]);
            } else {
                return redirect()->back()->withErrors(['Password Tidak Sama Dengan Yang Anda Masukkan']);
            }
        } else {
            return redirect()->back()->withErrors(['Password Lama Tidak Sama Dengan Yang Anda Masukkan']);
        }
        Log::changePassAct(Auth::guard('petugas')->user()->id);
        return redirect()->route('dashboard.index')->with([
            'success' => 'Password Berhasil Di Ubah',
        ]);
    }
}
