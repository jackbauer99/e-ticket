<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
use Hash;
use DB;
use App\Helper\Log;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\Role;
use App\Models\App;
use App\Models\Keluhan;
use App\Models\Ticket;
use App\Models\Settings;


class KeluhanController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = "Dashboard - Keluhan";
        $settings = Settings::all();
        $keluhan = Keluhan::all();
        $pelanggan = Pelanggan::all();
        $petugas = Petugas::all();
        return view('Dashboard.back.Keluhan.keluhan',compact(
            'title',
            'keluhan',
            'pelanggan',
            'petugas',
            'settings'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $title = "Keluhan Pelanggan";
        $settings = Settings::all();
        $pelanggan = Pelanggan::all();
        return view('Dashboard.back.Keluhan.create',compact(
            'title',
            'pelanggan',
            'settings'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        // Cek Auth petugas dulu untuk mengambil ID petugas
        if(Auth::guard('petugas')->check()) {
            $id_pet = Auth::guard('petugas')->user()->id;
        }

        // Tampilkan data petugas kantor
        $petugas = Petugas::where('id',$id_pet)->first();

        //dd($petugas->id);

        //
        $tgl = date('Ymd');

        // Generate no tiket
        $generate_ticket = rand(1,9);
        for($i=0;$i<9;$i++) {
            $generate_ticket .= rand(1,9);
        }

        // Tampilkan semua data Tiket
        $ticket = Ticket::all();
        // Hitung semua data Tiket
        $hitung = Ticket::all()->count();

        // Hitung dulu tiketnya jika = 0 maka tidak perlu dicek lagi kembar
        if($hitung == 0) {

            Ticket::create([
                'no_ticket' => $generate_ticket,
                'tiket_tanggal' => $tgl,
                'is_diambil' => 0
            ]);

        }
        // Jika lebih dari > 0
        else {
            // Ambil no tiket dulu
            foreach($ticket as $tc) {
                $t = $tc['no_ticket'];
            }
            // Cek apakah kembar no tiketnya
            if($t != $generate_ticket) {
                Ticket::create([
                    'no_ticket' => $generate_ticket,
                    'tiket_tanggal' => $tgl,
                    'is_diambil' => 0
                ]);
            }
        }

        // Tampilkan data tiket setelah di insert tadi
        $tiket = Ticket::all();

        // Ambil id tiketnya
        foreach($tiket as $hk) {
            $k = $hk['id'];
        }

        // Insert keluhan
        $insert = Keluhan::create([
            'pelanggan_id' => $request->pelanggan_nama,
            'ticket_id' => $k,
            'petugas_id' => $petugas->id,
            'keluhan_nama' => $request->keluhan_nama,
            'keluhan_tanggal' => $tgl,
        ]);

        if($insert) {
          Log::InsertLogAct(Auth::guard('petugas')->user()->id,'Keluhan');
          return redirect()->route('keluhan.index')->with([
              'success' => 'Penambahan Data Sukses',
          ]);
        } else {
          return redirect()->back()->withErrors(['Penambahan Data Keluhan Gagal']);
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $title = "Dashboard - Keluhan";
        $settings = Settings::all();
        $keluhan = Keluhan::where('id',$id)->get();
        $pelanggan = Pelanggan::all();
        $petugas = Petugas::all();
        return view('Dashboard.back.Keluhan.show',compact(
            'title',
            'keluhan',
            'pelanggan',
            'petugas',
            'settings'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $title = "Dashboard - Keluhan";
        $settings = Settings::all();
        $keluhan = Keluhan::where('id',$id)->get();
        $pelanggan = Pelanggan::all();
        $petugas = Petugas::all();
        return view('Dashboard.back.Keluhan.edit',compact(
            'title',
            'keluhan',
            'pelanggan',
            'petugas',
            'settings'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $keluhan = Keluhan::find($id);
        $update = $keluhan->update([
            'keluhan_nama' => $request->keluhan_nama,
        ]);

        if($update) {
          Log::UpdateLogAct(Auth::guard('petugas')->user()->id,'Keluhan');
          return redirect()->route('keluhan.index')->with([
              'success' => 'Keluhan Sukses Diubah',
          ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete_keluhan = Keluhan::where('id',$id)->first();
        $delete_keluhan->delete();
        Log::DeleteLogAct(Auth::guard('petugas')->user()->id,'Keluhan');
        return redirect()->route('keluhan.index')->with([
            'success' => 'Data Berhasil Dihapus',
        ]);
    }
}
