<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Realisasi;
use App\Models\Keluhan;

class Petugas extends Authenticatable
{
    //
    use softDeletes;
    use Notifiable;

    protected $table = 'petugas';

    protected $fillable = [
	    'role_id',
      'petugas_nip',
      'petugas_nama',
      'petugas_jabatan',
      'petugas_alamat',
		  'petugas_tanggal_lahir',
		  'petugas_jk',
      'petugas_foto',
      'petugas_username',
		  'petugas_password',
		  'is_diganti',
	];

	/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
	protected $hidden = [
        'password', 'remember_token',
    ];

	public function getAuthPassword()
	{
		return $this->petugas_password;
	}

    static public function storeRule($withPhoto = false)
	{
		$rules = [
			'petugas_nip' => 'unique:petugas,petugas_nip',
			'petugas_nama' => 'required',
			'petugas_jabatan' => 'required',
			'petugas_alamat' => 'required',
			'petugas_tanggal_lahir' => 'required',
		];

		if ($withPhoto) {
			$rules = Arr::add(
				$rules,
				'petugas_photo',
				'required|image|mimes:jpg,jpeg,png|max:2048'
			);
		}

		return $rules;
    }

    static public function updateRule($withPhoto = false)
	{
		$rules = [
			'users_name' => 'required',
			'users_npm' => 'required',
			'users_datebirth' => 'required',
			'users_email' => 'required',
			'users_gender' => 'required',
			'users_phone' => 'required',
			'users_address' => 'required',
		];

		if ($withPhoto) {
			$rules = Arr::add(
				$rules,
				'users_photo',
				'required|image|mimes:jpg,jpeg,png|max:2048'
			);
		}

		return $rules;
	}

	public function realisasi() {
        return $this->hasMany(Realisasi::class);
	}

	public function keluhan() {
		return $this->hasMany(Keluhan::class);
	}
}
