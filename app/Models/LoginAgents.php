<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Petugas;

class LoginAgents extends Model
{
    //
    protected $table = 'login_agents';

    protected $fillable = [
      'petugas_id',
	    'ipaddress',
      'operating_system',
      'webbrowser',
		  'devices',
      'activity',
		  'last_login_date'
    ];

    public function petugas() {
        return $this->belongsTo(Petugas::class,'petugas_id');
    }

}
