<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;

class App extends Model
{
    //
    static public function ValidationMessage()
	{
		$message = [
		  'required'  => ':attribute tidak boleh kosong',
		  'email'     => 'Format email salah',
		  'confirmed' => 'Tidak sama',
		  'min'       => ':attribute Minimal :min',
		  'max'       => ':attribute Maximal :max',
		  'unique'    => ':attribute Sudah digunakan',
		  'mimes'     => 'Format file salah',
		  'numeric'   => ':attribute harus numeric',
		  'same'      => ':other dan :attribute harus sama',
		  'image'     => ':attribute harus gambar',
		];

		return $message;
	}

	static public function compress($source, $quality)
	{
		$arr = explode('.', $source);
		unset($arr[count($arr)-1]);
		$destination = implode('', $arr).'.webp';

		$info = getimagesize($source);

		if ($info['mime'] == 'image/jpeg')
			$image = imagecreatefromjpeg($source);

		elseif ($info['mime'] == 'image/png')
			$image = imagecreatefrompng($source);

		// imagewebp($image, $destination, $quality);
		imagejpeg($image, $destination, $quality);

		if (filesize(public_path().'/'.$destination) % 2 == 1) {
		  file_put_contents(public_path().'/'.$destination, "\0", FILE_APPEND);
		}

		unlink($source);

		return '/'.$destination;
	}

	static public function image(
		$path,
		$request,
		$name = 'photo',
		$raw = false,
		$quality = 20,
		$width = 850
	)
	{

		if ($raw) {
			$file = $request;
		}else{
			$file = $request->file($name);
		}

		$pathPhoto = $file->store($path);

		$img = Image::make($pathPhoto)->getRealPath();
		if ($img->width() > $width) {
			$img->resize($width, null, function ($constraint) {
				$constraint->aspectRatio();
			});
			$img->save(public_path().'/'.$pathPhoto);
		}

		return App::compress($pathPhoto, $quality);
	}

}
