<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan;
use App\Models\Petugas;
use App\Models\Ticket;
use App\Models\Realisasi;

class Keluhan extends Model
{
    //

    protected $table = 'keluhan';

    protected $fillable = [
        'pelanggan_id',
        'petugas_id',
        'ticket_id',
        'keluhan_nama',
        'keluhan_tanggal',
        'foto_keluhan_1',
        'foto_keluhan_2',
        'foto_keluhan_3',
        'foto_keluhan_4',
    ];

    public function pelanggan() {
        return $this->belongsTo(Pelanggan::class,'pelanggan_id');
    }

    public function petugas() {
        return $this->belongsTo(Petugas::class,'petugas_id');
    }

    public function ticket() {
        return $this->belongsTo(Ticket::class,'ticket_id');
    }

    public function realisasi() {
        return $this->belongsTo(Realisasi::class,'ticket_id');
    }
}
