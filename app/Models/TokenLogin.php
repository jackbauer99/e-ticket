<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TokenLogin extends Model
{
    //
    protected $table = 'token_logins';

    protected $fillable = [
      'token',
      'expired_date',
    ];
}
