<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Keluhan;

class Pelanggan extends Authenticatable
{
    //
    use softDeletes;
    use Notifiable;

    protected $table = 'pelanggan';

    protected $fillable = [
	    'role_id',
      'pelanggan_id',
      'pelanggan_nama',
		  'pelanggan_alamat',
      'pelanggan_telepon',
		  'pelanggan_jk',
      'pelanggan_tanggal_lahir',
      'pelanggan_foto',
      'pelanggan_username',
      'pelanggan_password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
	protected $hidden = [
        'password', 'remember_token',
    ];

	public function getAuthPassword()
	{
		return $this->pelanggan_password;
	}

    static public function storeRule($withPhoto = false)
	{
		$rules = [
			'pelanggan_id' => 'unique:pelanggan,pelanggan_id',
			'pelanggan_nama' => 'required',
			'pelanggan_alamat' => 'required',
			'pelanggan_tanggal_lahir' => 'required',
		];

		if ($withPhoto) {
			$rules = Arr::add(
				$rules,
				'pelanggan_photo',
				'required|image|mimes:jpg,jpeg,png|max:2048'
			);
		}

		return $rules;
    }

    static public function updateRule($withPhoto = false)
	{
		$rules = [
			'users_name' => 'required',
			'users_npm' => 'required',
			'users_datebirth' => 'required',
			'users_email' => 'required',
			'users_gender' => 'required',
			'users_phone' => 'required',
			'users_address' => 'required',
		];

		if ($withPhoto) {
			$rules = Arr::add(
				$rules,
				'pelanggan_foto',
				'required|image|mimes:jpg,jpeg,png|max:2048'
			);
		}

		return $rules;
	}


    public function keluhan() {
        return $this->hasMany(Keluhan::class);
    }
}
