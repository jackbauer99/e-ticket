<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    //
    protected $table = 'settings';

    protected $fillable = [
       'dashboard_name',
       'login_image',
       'register_image',
    ];
}
