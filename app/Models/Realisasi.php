<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Ticket;
use App\Models\Petugas;
use App\Models\Keluhan;

class Realisasi extends Model
{
    //

    protected $table = 'realisasi';

    protected $fillable = [
	    'ticket_id',
      'petugas_id',
      'realisasi_nama',
		  'foto_realisasi_1',
		  'foto_realisasi_2',
      'foto_realisasi_3',
      'foto_realisasi_4',
      'realisasi_tanggal',
      'is_realisasi'
    ];

    public function ticket() {
        return $this->belongsTo(Ticket::class,'ticket_id');
    }

    public function petugas() {
        return $this->belongsTo(Petugas::class,'petugas_id');
    }

    public function keluhan() {
       return $this->belongsTo(Keluhan::class,'ticket_id');
    }

}
