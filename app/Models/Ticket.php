<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Realisasi;
use App\Models\Keluhan;

class Ticket extends Model
{
    //

    protected $table = 'tiket';

    protected $fillable = [
	    'no_ticket',
      'tiket_tanggal',
		  'tiket_tanggal_realisasi',
		  'tiket_diambil',
      'is_diambil',
    ];

    public function realisasi() {
        return $this->hasMany(Realisasi::class);
    }

    public function keluhan() {
        return $this->hasMany(Keluhan::class);
    }

}
