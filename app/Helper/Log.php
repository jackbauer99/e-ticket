<?php

/*
@author nugraha
*/

namespace App\Helper;

use App\Models\LoginAgents;
use Jenssegers\Agent\Agent;

class Log {

  public static function BeginInsertLog($petugas_id,$action) {

    $agent = new Agent();
    date_default_timezone_set("Asia/Jakarta");
    $log  = LoginAgents::create([
      'petugas_id' => $petugas_id,
      'ipaddress' => $_SERVER['REMOTE_ADDR'],
      'operating_system' => $agent->platform(),
      'webbrowser' => $agent->browser(),
      'devices' => $agent->device(),
      'activity' => $action,
      'last_login_date' => date('Y-m-d H:i:s'),
    ]);

    if($log) {
      $post_log = array(
        'status' => 'success'
      );
    } else {
      $post_log = array(
        'status' => 'failed'
      );
    }

    return $post_log;

  }

  public static function LoginLog($petugas_id) {
    self::BeginInsertLog($petugas_id,'Login');
  }

  public static function LogoutLog($petugas_id) {
    self::BeginInsertLog($petugas_id,'Logout');
  }

  public static function InsertLogAct($petugas_id,$act) {
    self::BeginInsertLog($petugas_id,'Insert Data '.$act);
  }

  public static function UpdateLogAct($petugas_id,$act) {
    self::BeginInsertLog($petugas_id,'Update Data '.$act);
  }

  public static function DeleteLogAct($petugas_id,$act) {
    self::BeginInsertLog($petugas_id,'Delete Data '.$act);
  }

  public static function setSettingsLogAct($petugas_id) {
    self::BeginInsertLog($petugas_id,'Mengatur Aplikasi');
  }

  public static function pickTicketAct($petugas_id) {
    self::BeginInsertLog($petugas_id,'Mengambil Tiket');
  }

  public static function changePassAct($petugas_id) {
    self::BeginInsertLog($petugas_id,'Mengganti Password');
  }

  public static function verification($petugas_id,$act) {
    if($act == 'down') {
      self::BeginInsertLog($petugas_id,'Membatalkan Realisasi');
    } else if($act == 'up') {
      self::BeginInsertLog($petugas_id,'Menyetujui Realisasi');
    }
  }

}


 ?>
