<!DOCTYPE html>
<html lang="en">

<head>

    @include('Dashboard.modules.meta')

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
        @include('Dashboard.modules.sidebar')
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
            @include('Dashboard.modules.nav')
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          

          <!-- Content Row -->
          @if(Auth::guard('petugas')->check())
            @if(Auth::guard('petugas')->user()->is_diganti == 0)
              <div class="alert alert-success">
                <h5>Password Anda Masih Default Silahkan Mengganti <a href = "{{ route('petugas.changePass',Auth::guard('petugas')->user()->id) }}">Disini !</a></h5>
              </div>
            @endif
          @endif    
          @yield('content') 
          
             
      <!-- End of Main Content -->

      <!-- Footer -->
            @include('Dashboard.modules.footer')
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Keluar Sistem ?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Silahkan Klik Tombol Keluar Untuk Keluar Dari Sesi</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
        <a class="btn btn-primary" href="{{route('auth.logout')}}">Keluar</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
    @include('Dashboard.modules.script')
  
</body>

</html>
