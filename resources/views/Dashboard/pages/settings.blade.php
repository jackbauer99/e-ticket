@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Settings</h1>
@if($msg = Session::get('success'))                                               
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif

<!--- Form -->
@if($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

@foreach($settings as $hasil)
<form method="post" action="{{route('dashboard.update',$hasil['id'])}}" enctype="multipart/form-data">
    {{csrf_field()}}
    {{method_field('PUT')}}
        <center>
            <br/>
            <br/>
        <div class="col-sm-5">
            <div class="form-group">
                <input type="text" class="form-control form-control-user" id="exampleFirstName" name="dashboard_name" placeholder="Nama Dashboard" value="{{ $hasil['dashboard_name'] }}">
            </div>
        <div class="form-group">
            <div class="form-grop text-left">
              <label for="BackgroundLogin">Backgorund Login</label>  
              <input type="file" class="form-control-file" name="login_image" placeholder="Tulis Sipnosis" onchange="preview1(this,'gambar1')">
            </div>
        </div>
            <div class="form-group">
                <img src="{{ $hasil['login_image'] }}" width = "250" class="rendered img-fluid mt-2" id="gambar1" alt=""/>
            </div>
        <div class="form-group">
            <div class="form-grop text-left">
                <label for="BackgroundRegister">Backgorund Register</label>  
                <input type="file" class="form-control-file" name="register_image" placeholder="Tulis Sipnosis" onchange="preview2(this,'gambar2')">
            </div>
        </div>
            <div class="form-group">
                <img src="{{ $hasil['register_image'] }}" width = "250" class="rendered img-fluid mt-2" id="gambar2" alt=""/>
            </div>
          <button class="btn btn-primary btn-user btn-block">
            Simpan
          </button>
        <hr>
</center>
</form>
@endforeach

<script charset="utf-8">

function preview1(gambar1,idpreview){
    var gb = gambar1.files;
        for (var i = 0; i < gb.length; i++){
            var gbPreview = gb[i];
            var imageType = /image.*/;
            var preview=document.getElementById(idpreview);
            var reader = new FileReader();
                if (gbPreview.type.match(imageType)) {
                    preview.file = gbPreview;
                    reader.onload = (function(element) {
                        return function(e) {
                            element.src = e.target.result;
                        };
                    })(preview);
                    reader.readAsDataURL(gbPreview);
                } else{
                    alert("Type file tidak sesuai. Khusus image.");
                }
            }
        }

function preview2(gambar2,idpreview){
    var gb = gambar2.files;
        for (var i = 0; i < gb.length; i++){
            var gbPreview = gb[i];
            var imageType = /image.*/;
            var preview=document.getElementById(idpreview);
            var reader = new FileReader();
                if (gbPreview.type.match(imageType)) {
                    preview.file = gbPreview;
                    reader.onload = (function(element) {
                        return function(e) {
                            element.src = e.target.result;
                        };
                    })(preview);
                    reader.readAsDataURL(gbPreview);
                } else{
                    alert("Type file tidak sesuai. Khusus image.");
                }
            }
        }        
          
</script>

@endsection
