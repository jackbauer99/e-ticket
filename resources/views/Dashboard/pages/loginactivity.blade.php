@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Aktivitas Login</h1>
@if($msg = Session::get('success'))
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="Pelanggan-tables" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Petugas NIP</th>
            <th>Nama Petugas</th>
            <th>IP Address</th>
            <th>Sistem Operasi</th>
            <th>Browser</th>
            <th>Devices</th>
            <th>Aktivitas</th>
            <th>Tanggal Aktivitas Terakhir</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Petugas NIP</th>
            <th>Nama Petugas</th>
            <th>IP Address</th>
            <th>Sistem Operasi</th>
            <th>Browser</th>
            <th>Devices</th>
            <th>Aktivitas</th>
            <th>Tanggal Aktivitas Terakhir</th>
          </tr>
        </tfoot>
        <tbody>
          @php $no = 1; @endphp
            @foreach($loginactivity as $hasil)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $hasil->petugas['petugas_nip'] }}</td>
                <td>{{ $hasil->petugas['petugas_nama'] }}</td>
                <td>{{ $hasil['ipaddress']}}</td>
                <td>{{ $hasil['operating_system']}}</td>
                <td>{{ $hasil['webbrowser'] }}</td>
                <td>{{ $hasil['devices'] }}</td>
                <td>{{ $hasil['activity'] }}</td>
                <td>{{ $hasil['last_login_date'] }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
  </div>
</div>
</div>
</div>

@endsection
