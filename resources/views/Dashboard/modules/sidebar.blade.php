<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('dashboard.index')}}">
  @foreach($settings as $hasil)
    @if($hasil['dashboard_name'] == NULL)
      @php $judul = "Dashboard Name" @endphp
    @else
      @php $judul = $hasil['dashboard_name'] @endphp
    @endif
    <div class="sidebar-brand-text mx-3">{{ $judul }}</div>
  @endforeach
</a>

  <!-- Divider -->
<hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
<li class="nav-item active">
  <a class="nav-link" href="{{route('dashboard.index')}}">
    <i class="fas fa-fw fa-tachometer-alt"></i>
    <span>Dashboard</span></a>
</li>

  <!-- Divider -->
<hr class="sidebar-divider">

  <!-- Heading -->
<div class="sidebar-heading">
  Master Data
</div>

  <!-- Nav Item - Pages Collapse Menu -->
@if(Auth::guard('petugas')->check())
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
    <i class="fas fa-fw fa-cog"></i>
    <span>Master Data</span>
  </a>
  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Master Data:</h6>
      @if(Auth::guard('petugas')->user()->role_id == 1)
        <a class="collapse-item" href="{{route('petugas.index')}}">Petugas</a>
        <a class="collapse-item" href="{{route('pelanggan.index')}}">Pelanggan</a>
        <a class="collapse-item" href="{{route('keluhan.index')}}">Keluhan</a>
        <a class="collapse-item" href="{{route('ticket.index')}}">Tiket</a>
        <a class="collapse-item" href="{{route('realisasi.index')}}">Realisasi</a>
      @elseif(Auth::guard('petugas')->user()->role_id == 2)
        <a class="collapse-item" href="{{route('pelanggan.index')}}">Pelanggan</a>
        <a class="collapse-item" href="{{route('keluhan.index')}}">Keluhan</a>
        <a class="collapse-item" href="{{route('ticket.index')}}">Tiket</a>
        <a class="collapse-item" href="{{route('realisasi.index')}}">Realisasi</a>
      @elseif(Auth::guard('petugas')->user()->role_id == 3)
        <a class="collapse-item" href="{{route('ticketact.index')}}">Ambil Tiket</a>
        <a class="collapse-item" href="{{route('realisasi.index')}}">Realisasi Anda</a>
      @endif
    </div>
  </div>
</li>
@endif

  <!-- Nav Item - Utilities Collapse Menu -->
@if(Auth::guard('pelanggan')->check())
<li class="nav-item">
  <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
    <i class="fas fa-fw fa-edit"></i>
    <span>Keluhan</span>
  </a>
  <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
    <div class="bg-white py-2 collapse-inner rounded">
      <h6 class="collapse-header">Keluhan</h6>
    <a class="collapse-item" href="{{route('keluhan.index')}}">Keluhan Anda</a>
    </div>
  </div>
</li>
@endif

  <!-- Nav Item - Utilities Collapse Menu -->
  @if(Auth::guard('petugas')->check())
    @if(Auth::guard('petugas')->user()->role_id == 1)
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
      <i class="fas fa-fw fa-edit"></i>
      <span>Pengaturan Dashboard</span>
    </a>
    <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Pengaturan Dashboard</h6>
      <a class="collapse-item" href="{{route('dashboard.settings')}}">Pengaturan Dashboard</a>
      </div>
    </div>
  </li>
    @endif
  @endif

  <!-- Nav Item - Utilities Collapse Menu -->
  @if(Auth::guard('petugas')->check())
    @if(Auth::guard('petugas')->user()->role_id == 1)
  <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities1" aria-expanded="true" aria-controls="collapseUtilities1">
      <i class="fas fa-fw fa-history"></i>
      <span>Aktivitas</span>
    </a>
    <div id="collapseUtilities1" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
      <div class="bg-white py-2 collapse-inner rounded">
        <h6 class="collapse-header">Aktivitas Login</h6>
      <a class="collapse-item" href="{{route('dashboard.loginactivity')}}">Aktivitas Login</a>
      </div>
    </div>
  </li>
    @endif
  @endif

</ul>
