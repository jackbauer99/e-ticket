<script src="{{asset('sb-admin-2/vendor/jquery/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('sb-admin-2/vendor/bootstrap/js/bootstrap.bundle.min.js')}}" type="text/javascript"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('sb-admin-2/vendor/jquery-easing/jquery.easing.min.js')}}" type="text/javascript"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('sb-admin-2/js/sb-admin-2.min.js')}}" type="text/javascript"></script>

<!-- Page level plugins -->
<script src="{{asset('sb-admin-2/vendor/chart.js/Chart.min.js')}}" type="text/javascript"></script>

<!-- Page level custom scripts -->
<script src="{{asset('sb-admin-2/js/demo/chart-area-demo.js')}}" type="text/javascript"></script>
<script src="{{asset('sb-admin-2/js/demo/chart-pie-demo.js')}}" type="text/javascript"></script>

<!-- Page level plugins -->
<script src="{{asset('sb-admin-2/vendor/datatables/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js')}}" type="text/javascript"></script>

<script src="{{asset('js/Dashboard/DataTables.js')}}" type="text/javascript"></script>
<script src="{{asset('js/Dashboard/Keluhan.js')}}" type="text/javascript"></script>

<script src = "{{asset('ckeditor/ckeditor.js')}}" type="text/javascript"></script>

<script src = "{{asset('select2/js/select2.min.js')}}" type="text/javascript"></script>

<script>
    CKEDITOR.replace( 'ckeditor' );
    CKEDITOR.replace( 'keluhan' );
</script>