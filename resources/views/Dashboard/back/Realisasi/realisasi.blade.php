@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
@if(Auth::guard('petugas')->check())
    @if(Auth::guard('petugas')->user()->role_id == 1 || Auth::guard('petugas')->user()->role_id == 2)
        @php $petugas = "Realisasi" @endphp
    @elseif(Auth::guard('petugas')->user()->role_id == 3)
        @php $petugas = "Realisasi Anda" @endphp
    @endif
@endif

<h1 class="h3 mb-2 text-gray-800">{{ $petugas }}</h1>
@if($msg = Session::get('success'))
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif

<p class="mb-4"></p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="Petugas-tables" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Nomor Tiket</th>
            <th>Nama Petugas</th>
            <th>Nama Realisasi</th>
            <th>Foto Realisasi 1</th>
            <th>Foto Realisasi 2</th>
            <th>Foto Realisasi 3</th>
            <th>Foto Realisasi 4</th>
            <th>Tanggal Realisasi</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Nomor Tiket</th>
            <th>Nama Petugas</th>
            <th>Nama Realisasi</th>
            <th>Foto Realisasi 1</th>
            <th>Foto Realisasi 2</th>
            <th>Foto Realisasi 3</th>
            <th>Foto Realisasi 4</th>
            <th>Tanggal Realisasi</th>
            <th>Aksi</th>
          </tr>
        </tfoot>
        <tbody>
            @php $no = 1; @endphp
            @foreach($realisasi as $hasil)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $hasil->ticket['no_ticket'] }}</td>
                <td>{{ $hasil->petugas['petugas_nama'] }}</td>
                <td>{{ $hasil['realisasi_nama'] }}</td>
                <td><img width="100" src = "{{ asset($hasil['foto_realisasi_1']) }}"></td>
                <td><img width="100" src = "{{ asset($hasil['foto_realisasi_2']) }}"></td>
                <td><img width="100" src = "{{ asset($hasil['foto_realisasi_3']) }}"></td>
                <td><img width="100" src = "{{ asset($hasil['foto_realisasi_4']) }}"></td>
                <td>{{ $hasil->realisasi_tanggal }}</td>
                <form action="{{route('realisasi.destroy' ,$hasil->id)}}" method="post">
                  {{csrf_field()}}
                  {{method_field('DELETE')}}
                  <td>
                    @if(Auth::guard('petugas')->check())
                      @if(Auth::guard('petugas')->user()->petugas_jabatan == "Petugas Kantor")
                      <center><button type="submit" class="btn btn-danger btn-sm" name="_method" value="Delete" onclick="return confirm('Yakin Hapus Data ??')"><i class="fa fa-trash"></i></button></form>
                      @endif
                    @endif

                    @if($hasil->is_realisasi == 0)
                      @if(Auth::guard('petugas')->check())
                        @if(Auth::guard('petugas')->user()->role_id == 1 || Auth::guard('petugas')->user()->role_id == 2)
                          <button class="btn btn-danger btn-sm"><i class="fa fa-window-close"></i></button>
                        @endif
                      @endif
                      <a href="{{route('realisasi.edit',$hasil->id)}}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a></td>
                    @elseif($hasil->is_realisasi == 1)
                      @if(Auth::guard('petugas')->check())
                        @if(Auth::guard('petugas')->user()->role_id == 1 || Auth::guard('petugas')->user()->role_id == 2)
                          <a href = "{{ route('realisasi.ver', $hasil->id) }}" class="btn btn-warning btn-sm" onclick="return confirm('Apakah Anda Akan Konfirmasi Realisasi ??')"><i class="fa fa-check"></i></a>
                        @endif
                      @endif
                    @elseif($hasil->is_realisasi == 2)
                      @if(Auth::guard('petugas')->check())
                        @if(Auth::guard('petugas')->user()->role_id == 1 || Auth::guard('petugas')->user()->role_id == 2)
                          <a href = "{{ route('realisasi.downver', $hasil->id) }}" class="btn btn-success btn-sm" onclick="return confirm('Apakah Anda Akan Membatalkan Konfirmasi Realisasi ??')"><i class="fa fa-check"></i></a>
                        @elseif(Auth::guard('petugas')->user()->role_id == 3)
                          <button class="btn btn-success btn-sm"><i class="fa fa-check"></i></button>
                        @endif
                      @endif
                    @endif
                  </td>
              </tr>
            @endforeach
        </tbody>
    </table>
  </div>
</div>
</div>
</div>

@endsection
