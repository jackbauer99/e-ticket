@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Realisasi</h1>
@if($msg = Session::get('success'))                                               
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif

<!--- Form -->
@if($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

@foreach($realisasi as $hasil)
<form method="post" action="{{route('realisasi.update',$hasil['id'])}}" enctype="multipart/form-data">
    {{csrf_field()}}
    {{method_field('PUT')}}
        <center>
            <br/>
            <br/>
        <div class="col-sm-5">
          <div class="form-group">
          </div>
          <div class="form-group">
            @foreach($hasil->ticket->keluhan as $kel)
              <textarea class="form-control form-control-user" id="keluhan" readonly name="keluhan" placeholder="Alamat">{{ $kel['keluhan_nama'] }}</textarea>
            @endforeach
          </div>
          <div class="form-group">
          <textarea class="form-control form-control-user" id="ckeditor" name="realisasi_nama" placeholder="Alamat">{{ $hasil['realisasi_nama'] }}</textarea>
          </div>
          <div class="form-group">
            <div class="form-grop text-left">
            <input type="file" name="realisasi_foto_1" value="{{ $hasil['realisasi_foto_1'] }}" placeholder="Tulis Sipnosis" onchange="preview(this,'gambar')">
            </div>
        </div>
        <div class="form-group">
            <div class="form-grop text-left">
              <input type="file" name="realisasi_foto_2" placeholder="Tulis Sipnosis" onchange="preview(this,'gambar')">
            </div>
        </div>
        <div class="form-group">
            <div class="form-grop text-left">
              <input type="file" name="realisasi_foto_3" placeholder="Tulis Sipnosis" onchange="preview(this,'gambar')">
            </div>
        </div>
        <div class="form-group">
            <div class="form-grop text-left">
              <input type="file" name="realisasi_foto_4" placeholder="Tulis Sipnosis" onchange="preview(this,'gambar')">
            </div>
        </div>
          <button class="btn btn-primary btn-user btn-block" onclick="return confirm('Apakah Data Yang Akan Dikirim Sudah Benar ??')">
            Kirim Realisasi
          </button>
        <hr>
</center>
</form>
@endforeach

@endsection
