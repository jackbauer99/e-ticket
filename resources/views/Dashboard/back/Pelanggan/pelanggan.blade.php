@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Pelanggan</h1>
@if($msg = Session::get('success'))
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif
<a href="{{route('pelanggan.create')}}" button type="button" class="btn btn-primary pull-right">
  <span class="fa fa-plus"></span> Tambah Data</a>
<p class="mb-4"></p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="Pelanggan-tables" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Foto</th>
            <th>ID Pelanggan</th>
            <th>Nama Pelanggan</th>
            <th>No Telepon Pelanggan</th>
            <th>Alamat Pelanggan</th>
            <th>Tanggal Lahir Pelanggan</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Foto</th>
            <th>ID Pelanggan</th>
            <th>Nama Pelanggan</th>
            <th>No Telepon Pelanggan</th>
            <th>Alamat Pelanggan</th>
            <th>Tanggal Lahir Pelanggan</th>
            <th>Aksi</th>
          </tr>
        </tfoot>
        <tbody>
          @php $no = 1; @endphp
            @foreach($pelanggan as $hasil)
            <tr>
              <td>{{ $no++ }}</td>
                <td><img width = "100" src = "{{ asset($hasil['pelanggan_foto']) }}"></td>
                <td>{{ $hasil['pelanggan_id'] }}</td>
                <td>{{ $hasil['pelanggan_nama']}}</td>
                <td>{{ $hasil['pelanggan_telepon']}}</td>
                <td>{{ $hasil['pelanggan_alamat'] }}</td>
                <td>{{ $hasil['pelanggan_tanggal_lahir'] }}</td>
                <form action="{{route('pelanggan.destroy' ,$hasil['id'])}}" method="post">
                  {{csrf_field()}}
                  {{method_field('DELETE')}}
                  <td><center><button type="submit" class="btn btn-danger btn-sm" name="_method" value="Delete" onclick="return confirm('Yakin Hapus Data ??')"><i class="fa fa-trash"></i></button></form>
                  <a href="{{route('pelanggan.edit',$hasil['id'])}}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
  </div>
</div>
</div>
</div>

@endsection
