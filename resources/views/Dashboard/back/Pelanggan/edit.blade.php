@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Pelanggan</h1>
@if($msg = Session::get('success'))
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif

<!--- Form -->
@if($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

@foreach($pelanggan as $pelanggans)
<form method="post" action="{{route('pelanggan.update',$pelanggans['id'])}}" enctype="multipart/form-data">
    {{csrf_field()}}
    {{method_field('PUT')}}
        <center>
            <br/>
            <br/>
        <div class="col-sm-5">
          <div class="form-group">
          <input type="text" class="form-control form-control-user" id="exampleFirstName" name="pelanggan_nama" placeholder="Nama Anda" value="{{ $pelanggans['pelanggan_nama'] }}">
          </div>
        <div class="form-group">
        <textarea class="form-control form-control-user" id="exampleInputAlamat" name="pelanggan_alamat" placeholder="Alamat">{{ $pelanggans['pelanggan_alamat'] }}</textarea>
        </div>
          <div class="form-group">
          <input type="date" class="form-control form-control-user" id="exampleInputtgl" name="pelanggan_tanggal_lahir" placeholder="Tanggal Lahir" value="{{ $pelanggans['pelanggan_tanggal_lahir'] }}">
          </div>
          <div class="form-group">
            <input type="number" class="form-control form-control-user" id="exampleInputtlpn" name="pelanggan_telepon" placeholder="Nomor Telepon" value="{{ $pelanggans['pelanggan_telepon'] }}">
          </div>
          <div class = "form-group">
            <select name="pelanggan_jk" class="form-control form-control-user">
              <option value="">Pilih</option>
                @if($pelanggans['pelanggan_jk'] == "L")
                    <option value= "L" selected>Laki-laki</option>
                    <option value= "P">Perempuan</option>
                @elseif($pelanggans['pelanggan_jk'] == "P")
                    <option value= "P" selected>Perempuan</option>
                    <option value= "L">Laki-laki</option>
                @elseif($pelanggans['pelanggan_jk'] == NULL)
                    <option value= "L">Laki-laki</option>
                    <option value= "P">Perempuan</option>
                @endif
              </select>
          </div>
          <div class="form-group">
            <div class="form-grop text-left">
              <input type="file" name="pelanggan_foto" placeholder="Tulis Sipnosis" onchange="preview(this,'gambar')">
            </div>
        </div>
        <div class="form-group">
        <img src="{{ asset($pelanggans['pelanggan_foto']) }}" width = "250" class="rendered img-fluid mt-2" id="gambar" alt=""/>
        </div>
        <button class="btn btn-primary btn-user btn-block">
          Simpan
        </button>
        <hr>
</center>
</form>
@endforeach

<script charset="utf-8">

  function preview(gambar,idpreview){
        var gb = gambar.files;
              for (var i = 0; i < gb.length; i++){
                  var gbPreview = gb[i];
                  var imageType = /image.*/;
                  var preview=document.getElementById(idpreview);
                  var reader = new FileReader();
                      if (gbPreview.type.match(imageType)) {
                          preview.file = gbPreview;
                          reader.onload = (function(element) {
                              return function(e) {
                                  element.src = e.target.result;
                              };
                          })(preview);
                          reader.readAsDataURL(gbPreview);
                      } else{
                          alert("Type file tidak sesuai. Khusus image.");
                      }
                  }
              }

        </script>

@endsection
