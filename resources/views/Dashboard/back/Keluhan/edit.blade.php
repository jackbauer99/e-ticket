@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Pelanggan</h1>
@if($msg = Session::get('success'))                                               
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif

<!--- Form -->
@if($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif
@foreach($keluhan as $hasil)		
<form method="post" action="{{route('keluhan.update',$hasil['id'])}}" enctype="multipart/form-data">
  {{csrf_field()}}
  {{method_field('PUT')}}
      <center>
            <br/>
            <br/>
        <div class="col-sm-5">
          <div class="form-group">
          <select class="form-control form-control-user" id="src2" name="pelanggan_nama">
            @foreach($pelanggan as $pl)
              <option value=""></option>  
              @if($hasil->pelanggan['id'] == $pl['id'])
                <option value="{{$pl['id']}}" selected>{{$pl['pelanggan_id']." - ".$pl['pelanggan_nama']}}</option>
              @elseif($hasil->pelanggan['pelanggan_id'] != $pl['id'])  
                <option value="{{$pl['id']}}">{{$pl['pelanggan_id']." - ".$pl['pelanggan_nama']}}</option>
              @endif
            @endforeach
          </select>
          </div>
          <div class="form-group">
            <textarea class="form-control form-control-user" id="ckeditor" name="keluhan_nama" placeholder="Alamat">{{ $hasil['keluhan_nama'] }}</textarea>
          </div>
          <button class="btn btn-primary btn-user btn-block">
            Simpan Keluhan
          </button>
        <hr>
      </center>
</form>
@endforeach

@endsection
