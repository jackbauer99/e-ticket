@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Pelanggan</h1>
@if($msg = Session::get('success'))                                               
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif

<!--- Form -->
@if($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif		
<form method="post" action="{{route('keluhan.store')}}" enctype="multipart/form-data">
	{{csrf_field()}}
      <center>
            <br/>
            <br/>
        <div class="col-sm-5">
          <div class="form-group">
          <select class="form-control form-control-user" id="src" name="pelanggan_nama">
            @foreach($pelanggan as $hasil)
              <option value=""></option>
              <option value="{{$hasil['id']}}">{{$hasil['pelanggan_id']." - ".$hasil['pelanggan_nama']}}</option>
            @endforeach
          </select>
          </div>
          <div class="form-group">
            <textarea class="form-control form-control-user" id="ckeditor" name="keluhan_nama" placeholder="Alamat"></textarea>
          </div>
          <button class="btn btn-primary btn-user btn-block">
            Simpan Keluhan
          </button>
        <hr>
      </center>
</form>

@endsection
