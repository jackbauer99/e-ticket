@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->

@if(Auth::guard('pelanggan')->check())
@php $nama = "Keluhan ".Auth::guard('pelanggan')->user()->pelanggan_nama; @endphp
@elseif(Auth::guard('petugas')->check())
@php $nama = "Keluhan"; @endphp
@endif

<h1 class="h3 mb-2 text-gray-800">{{ $nama }}</h1>
@if($msg = Session::get('success'))                                               
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif
<a href="{{route('keluhan.create')}}" button type="button" class="btn btn-primary pull-right">
  <span class="fa fa-plus"></span> Tambah Data</a>
<p class="mb-4"></p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="Pelanggan-tables" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Pelanggan</th>
            <th>Daftar Keluhan</th>
            <th>Tanggal Keluhan</th>
            <th>No Telpon Keluhan</th>
            <th>Alamat Keluhan</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Nama Pelanggan</th>
            <th>Daftar Keluhan</th>
            <th>Tanggal Keluhan</th>
            <th>No Telpon Keluhan</th>
            <th>Alamat Keluhan</th>
            <th>Aksi</th>
          </tr>
        </tfoot>
        <tbody>
            @php $no = 1; @endphp
            @foreach($keluhan as $hasil)
            <tr>    
                <td>{{ $no++ }}</td>
                <td>{{ $hasil->pelanggan['pelanggan_nama'] }}</td>
                <td>{{ $hasil['keluhan_nama'] }}</td>
                <td>{{ $hasil['keluhan_tanggal'] }}</td>
                <td>{{ $hasil->pelanggan['pelanggan_telepon'] }}</td>
                <td>{{ $hasil->pelanggan['pelanggan_alamat'] }}</td>
                <form action="{{route('keluhan.destroy' ,$hasil['id'])}}" method="post">
                  {{csrf_field()}}
                  {{method_field('DELETE')}}
                  <td><center><button type="submit" class="btn btn-danger btn-sm" name="_method" value="Delete" onclick="return confirm('Yakin Hapus Data ??')"><i class="fa fa-trash"></i></button></form>
                   <a href="{{route('keluhan.edit',$hasil['id'])}}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                   <a href="{{route('keluhan.show',$hasil['id'])}}" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a></td>
            </tr>    
            @endforeach    
        </tbody>
    </table>
  </div>
</div>
</div>
</div>
  
@endsection
