@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Petugas</h1>
@if($msg = Session::get('success'))                                               
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif

<!--- Form -->
@if($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			    <li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif	

@foreach($petugas as $hasil)
<form method="post" action="{{route('petugas.password',$hasil['id'])}}" enctype="multipart/form-data">
    {{csrf_field()}}
    {{method_field('PUT')}}
        <center>
            <br/>
            <br/>
        <div class="col-sm-5">
            <div class="form-group">
                <div class="form-grop text-left">
                    <input type="password" class="form-control form-control-user" name="old_password" placeholder="Tulis Password Lama Anda">
                </div>
            </div>
            <div class="form-group">
                <div class="form-grop text-left">
                    <input type="password" class="form-control form-control-user" name="new_password" placeholder="Tulis Password Baru Anda">
                </div>
            </div>
            <div class="form-group">
                <div class="form-grop text-left">
                    <input type="password" class="form-control form-control-user" name="new_password_repeat" placeholder="Tulis Password Baru Anda Sekali Lagi">
                </div>
            </div>
        <button class="btn btn-primary btn-user btn-block">
          <i class = "fa fa-save"></i> Simpan
        </button>
        <hr>
    </div>  
</center>
</form>
@endforeach
  

@endsection


