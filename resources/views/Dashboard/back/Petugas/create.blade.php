@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Petugas</h1>
@if($msg = Session::get('success'))                                               
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif

<!--- Form -->
@if($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif	

<form method="post" action="{{route('petugas.store')}}" enctype="multipart/form-data">
	{{csrf_field()}}
        <center>
            <br/>
            <br/>
        <div class="col-sm-5">
          <div class="form-group">
            <input type="text" class="form-control form-control-user" id="exampleFirstName" name="petugas_nama" placeholder="Nama Petugas">
          </div>
          <div class="form-group">
              <select name="petugas_jabatan" class="form-control form-control-user">
                  <option value="">Pilih Jabatan</option>
                  <option value="1">Admin</option>
                  <option value="2">Petugas Kantor</option>
                  <option value="3">Petugas Lapangan</option>
              </select>
            </div>  
        <div class="form-group">
          <textarea class="form-control form-control-user" id="exampleInputAlamat" name="petugas_alamat" placeholder="Alamat"></textarea>
        </div>
          <div class="form-group">
            <input type="date" class="form-control form-control-user" id="exampleInputtgl" name="petugas_tanggal_lahir" placeholder="Tanggal Lahir">
          </div>
          <div class = "form-group">
            <select name="petugas_jk" class="form-control form-control-user">
              <option value="">Pilih Jenis Kelamin</option>
                <option value="L">Laki-laki</option>
                <option value="P">Perempuan</option>
              </select>
          </div>    
          <div class="form-group">
            <div class="form-grop text-left">
              <input type="file" name="petugas_foto" placeholder="Tulis Sipnosis" onchange="preview(this,'gambar')">
            </div>
        </div>
        <div class="form-group">
            <img src="" width = "250" class="rendered img-fluid mt-2" id="gambar" alt=""/>
        </div>
        <button class="btn btn-primary btn-user btn-block">
          <i class = "fa fa-save"></i> Simpan
        </button>
        <hr>
</center>
</form>
  
<script charset="utf-8">

  function preview(gambar,idpreview){
        var gb = gambar.files;
              for (var i = 0; i < gb.length; i++){
                  var gbPreview = gb[i];
                  var imageType = /image.*/;
                  var preview=document.getElementById(idpreview);
                  var reader = new FileReader();
                      if (gbPreview.type.match(imageType)) {
                          preview.file = gbPreview;
                          reader.onload = (function(element) {
                              return function(e) {
                                  element.src = e.target.result;
                              };
                          })(preview);
                          reader.readAsDataURL(gbPreview);
                      } else{
                          alert("Type file tidak sesuai. Khusus image.");
                      }
                  }
              }
        
        </script>

@endsection


