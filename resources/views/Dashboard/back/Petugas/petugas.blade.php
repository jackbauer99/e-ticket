@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Petugas</h1>
@if($msg = Session::get('success'))
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif
<a href="{{route('petugas.create')}}" button type="button" class="btn btn-primary pull-right">
  <span class="fa fa-plus"></span> Tambah Data</a>
<p class="mb-4"></p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="Petugas-tables" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>Foto</th>
            <th>NIP</th>
            <th>Nama Petugas</th>
            <th>Jabatan Petugas</th>
            <th>Alamat Petugas</th>
            <th>Tanggal Lahir Petugas</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>Foto</th>
            <th>NIP</th>
            <th>Nama Petugas</th>
            <th>Jabatan Petugas</th>
            <th>Alamat Petugas</th>
            <th>Tanggal Lahir Petugas</th>
            <th>Aksi</th>
          </tr>
        </tfoot>
        <tbody>
            @foreach($petugas as $hasil)
            <tr>
                <td><img width="100" src = "{{ asset($hasil['petugas_foto']) }}"></td>
                <td>{{ $hasil['petugas_nip'] }}</td>
                <td>{{ $hasil['petugas_nama']}}</td>
                <td>{{ $hasil['petugas_jabatan'] }}</td>
                <td>{{ $hasil['petugas_alamat'] }}</td>
                <td>{{ $hasil['petugas_tanggal_lahir'] }}</td>
                <form action="{{route('petugas.destroy' ,$hasil['id'])}}" method="post">
                  {{csrf_field()}}
                  {{method_field('DELETE')}}
                  <td><center><button type="submit" class="btn btn-danger btn-sm" name="_method" value="Delete" onclick="return confirm('Yakin Hapus Data ??')"><i class="fa fa-trash"></i></button></form>
                  <a href="{{route('petugas.edit',$hasil['id'])}}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
  </div>
</div>
</div>
</div>

@endsection
