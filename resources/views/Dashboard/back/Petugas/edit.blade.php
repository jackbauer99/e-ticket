@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Petugas</h1>
@if($msg = Session::get('success'))
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif

<!--- Form -->
@if($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
	</div>
@endif

@foreach($petugas as $hasil)
<form method="post" action="{{route('petugas.update',$hasil['id'])}}" enctype="multipart/form-data">
    {{csrf_field()}}
    {{method_field('PUT')}}
        <center>
            <br/>
            <br/>
        <div class="col-sm-5">
          <div class="form-group">
          <input type="text" class="form-control form-control-user" id="exampleFirstName" name="petugas_nama" placeholder="Nama Petugas" value="{{ $hasil['petugas_nama'] }}">
          </div>
          <div class="form-group">
              <select name="petugas_jabatan" class="form-control form-control-user">
                  <option value="">Pilih Jabatan</option>
                  @if($hasil['petugas_jabatan'] == "Admin")
                    <option value="1" selected>Admin</option>
                    <option value="2">Petugas Kantor</option>
                    <option value="3">Petugas Lapangan</option>
                  @elseif($hasil['petugas_jabatan'] == "Petugas Kantor")
                    <option value="1">Admin</option>
                    <option value="2" selected>Petugas Kantor</option>
                    <option value="3">Petugas Lapangan</option>
                  @elseif($hasil['petugas_jabatan'] == "Petugas Lapangan")
                    <option value="1">Admin</option>
                    <option value="2">Petugas Kantor</option>
                    <option value="3" selected>Petugas Lapangan</option>
                  @endif
              </select>
            </div>
        <div class="form-group">
        <textarea class="form-control form-control-user" id="exampleInputAlamat" name="petugas_alamat" placeholder="Alamat">{{ $hasil['petugas_alamat'] }}</textarea>
        </div>
          <div class="form-group">
          <input type="date" class="form-control form-control-user" id="exampleInputtgl" name="petugas_tanggal_lahir" placeholder="Tanggal Lahir" value="{{ $hasil['petugas_tanggal_lahir'] }}">
          </div>
          <div class = "form-group">
            <select name="petugas_jk" class="form-control form-control-user">
              <option value="">Pilih Jenis Kelamin</option>
                @if($hasil['petugas_jk'] == "L")
                    <option value="L" selected>Laki-laki</option>
                    <option value="P">Perempuan</option>
                @elseif($hasil['petugas_jk'] == "P")
                    <option value="L">Laki-laki</option>
                    <option value="P" selected>Perempuan</option>
                @elseif($hasil['petugas_jk'] == NULL)
                    <option value= "L">Laki-laki</option>
                    <option value= "P">Perempuan</option>
                @endif
              </select>
          </div>
          <div class="form-group">
            <div class="form-grop text-left">
              <input type="file" name="petugas_foto" placeholder="Tulis Sipnosis" onchange="preview(this,'gambar')">
            </div>
        </div>
        <div class="form-group">
          <img src="{{ asset($hasil['petugas_foto']) }}" width = "250" class="rendered img-fluid mt-2" id="gambar" alt=""/>
        </div>
        <button class="btn btn-primary btn-user btn-block">
          <i class = "fa fa-save"></i> Simpan
        </button>
        <hr>
</center>
</form>
@endforeach

<script charset="utf-8">

  function preview(gambar,idpreview){
        var gb = gambar.files;
              for (var i = 0; i < gb.length; i++){
                  var gbPreview = gb[i];
                  var imageType = /image.*/;
                  var preview=document.getElementById(idpreview);
                  var reader = new FileReader();
                      if (gbPreview.type.match(imageType)) {
                          preview.file = gbPreview;
                          reader.onload = (function(element) {
                              return function(e) {
                                  element.src = e.target.result;
                              };
                          })(preview);
                          reader.readAsDataURL(gbPreview);
                      } else{
                          alert("Type file tidak sesuai. Khusus image.");
                      }
                  }
              }

        </script>

@endsection
