@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Pelanggan</h1>
@if($msg = Session::get('success'))                                               
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif

@if($msg = Session::get('failed'))                                               
  <div class="alert alert-danger">
    {{ $msg }}
  </div>
@endif

@if($msg = Session::get('warning'))                                               
  <div class="alert alert-warning">
    {{ $msg }}
  </div>
@endif

<!--- Form -->

     <center>
            <br/>
            <br/>
        <div class="col-sm-5">
        <a href = "{{ route('ticketact.create') }}" class="btn btn-primary btn-user btn-block">
         Ambil Tiket
        </a>
        <hr>
    </center>

  
@endsection
