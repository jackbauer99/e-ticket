@extends('Dashboard.app')

@section('content')

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Tiket</h1>
@if($msg = Session::get('success'))                                               
  <div class="alert alert-success">
    {{ $msg }}
  </div>
@endif

<p class="mb-4"></p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="Petugas-tables" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Nomor Tiket</th>
            <th>Tanggal Tiket</th>
            <th>Tanggal Diambil</th>
            <th>Tanggal Tiket Realisasi</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Nomor Tiket</th>
            <th>Tanggal Tiket</th>
            <th>Tanggal Diambil</th>
            <th>Tanggal Tiket Realisasi</th>
            <th>Aksi</th>
          </tr>
        </tfoot>
        <tbody>
            @php $no = 1; @endphp 
            @foreach($ticket as $hasil)
            <tr>    
                <td>{{ $no++ }}</td>
                <td>{{ $hasil['no_ticket'] }}</td>
                <td>{{ $hasil['tiket_tanggal'] }}</td>
                <td>{{ $hasil['tiket_diambil'] }}</td>
                <td>{{ $hasil['tiket_tanggal_realisasi'] }}</td>
                <form action="{{route('ticket.destroy' ,$hasil['id'])}}" method="post">
                  {{csrf_field()}}
                  {{method_field('DELETE')}}
                  <td><center><button type="submit" class="btn btn-danger btn-sm" name="_method" value="Delete" onclick="return confirm('Yakin Hapus Data ??')"><i class="fa fa-trash"></i></button></form>
                    @if($hasil['is_diambil'] == 0)
                        <button class="btn btn-danger btn-sm"><i class="fa fa-window-close"></i></button>
                    @elseif($hasil['is_diambil'] == 1)
                        <button class="btn btn-success btn-sm"><i class="fa fa-check"></i></button>
                    @endif        
            </tr>    
            @endforeach    
        </tbody>
    </table>
  </div>
</div>
</div>
</div>
  
@endsection
