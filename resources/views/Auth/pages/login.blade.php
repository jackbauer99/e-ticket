@include('Auth.modules.header')

@foreach($settings as $hasil)
@php $image = asset($hasil['login_image']); @endphp
<style>
.bg-login-image {
  background: url({{ $image }});
  background-position: center;
  background-size: cover;
}
</style>
@endforeach

<body class="bg-gradient-primary">

    <div class="container">

      <!-- Outer Row -->
      <div class="row justify-content-center">
        <div class="col-xl-10 col-lg-12 col-md-9">
          <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">

              <!-- Card Body Gambar -->
              <div class="row">
                <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                <div class="col-lg-6">
                  <div class="p-5">

                    <!-- Menampilkan pesan error diambil dari validate -->
                    @if ($errors->any())
                      <div class="alert alert-danger">
                        <ul class="mb-0">
                      @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                      @endforeach
                      </ul>
                      </div>
                    @endif

                    <!-- Form Login -->
                    <div class="text-center">
                      <h1 class="h4 text-gray-900 mb-4">Selamat Datang</h1>
                    </div>
                    <form class="user" action="{{route('auth.auth')}}" method="POST" enctype="multipart/form-data">
                      {{csrf_field()}}
                      <div class="form-group">
                        <input type="text" name = "user_username" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="NIP">
                      </div>
                      <div class="form-group">
                        <input type="password" name= "user_userpassword" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password">
                      </div>
                      <button type = "submit" class="btn btn-primary btn-user btn-block">
                        Login
                      </button>
                      <hr>
                    </form>
                    <hr>
                    <div class="text-center">
                      <!--<a class="small" href="{{route('auth.register')}}">Buat Akun</a>-->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>

    </div>

    @include('Auth.modules.footer')
