
@include('Auth.modules.header')

@foreach($settings as $hasil)
@php $image = asset($hasil['register_image']); @endphp
<style>
.bg-register-image {
background: url({{ $image }});
background-position: center;
background-size: cover;
}
</style>
@endforeach


<body class="bg-gradient-primary">

    <div class="container">


    @if($msg = Session::get('success'))
      <div class="alert alert-success">
        {{ $msg }}
      </div>
    @endif

      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
            <div class="col-lg-7">
              <div class="p-5">
                @if ($errors->any())
                  <div class="alert alert-danger">
                    <ul class="mb-0">
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                  </ul>
                  </div>
                @endif
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">Buat Akun</h1>
                </div>
              <form class="user" action="{{route('auth.store')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" id="exampleFirstName" name="pelanggan_nama" placeholder="Nama Anda">
                    </div>
                  <div class="form-group">
                    <textarea class="form-control form-control-user" id="exampleInputAlamat" name="pelanggan_alamat" placeholder="Alamat"></textarea>
                  </div>
                    <div class="form-group">
                      <input type="date" class="form-control form-control-user" id="exampleInputtgl" name="pelanggan_tanggal_lahir" placeholder="Tanggal Lahir">
                    </div>
                    <div class="form-group">
                      <div class="form-grop text-left">
                        <input type="file" name="pelanggan_foto" placeholder="Tulis Sipnosis" onchange="preview(this,'gambar')">
                      </div>
                  </div>
                  <div class="form-group">
                      <img src="" width = "250" class="rendered img-fluid mt-2" id="gambar" alt=""/>
                  </div>
                  <button class="btn btn-primary btn-user btn-block">
                    Daftar Baru
                  </button>
                  <hr>
                </form>
                <hr>
                <div class="text-center">
                Sudah Punya Akun ? <a class="small" href="{{route('auth.login')}}">Silahkan Login</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </body>

    @include('Auth.modules.footer')

    <script charset="utf-8">

function preview(gambar,idpreview){
    	var gb = gambar.files;
            for (var i = 0; i < gb.length; i++){
                var gbPreview = gb[i];
                var imageType = /image.*/;
                var preview=document.getElementById(idpreview);
                var reader = new FileReader();
                    if (gbPreview.type.match(imageType)) {
                        preview.file = gbPreview;
                        reader.onload = (function(element) {
                            return function(e) {
                                element.src = e.target.result;
                            };
                        })(preview);
                        reader.readAsDataURL(gbPreview);
                    } else{
                        alert("Type file tidak sesuai. Khusus image.");
                    }
                }
            }

      </script>
